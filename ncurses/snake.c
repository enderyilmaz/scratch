#include <ncurses.h>
#define N 1024

typedef enum  { LEFT=0, RIGHT=1, UP=2, DOWN=3 }dir;

int snake_x[N];
int snake_y[N];
int snake_head;
int snake_tail;


int main() {
    int c=0;
    int x,y;
    dir move_dir = RIGHT;
    x=5;
    y=5;

    snake_x[0] = 0;
    snake_y[0] = 5;
    snake_x[1] = 5;
    snake_y[1] = 5;
    snake_head = 1;
    snake_tail = 0;

    initscr();

    nodelay(stdscr, TRUE);
    noecho();

    move(y,x);
    addch('*');
    refresh();
    keypad(stdscr, TRUE);
    while(TRUE) {
        int is_growing = 0;
        c = getch();
        if (c=='q')
            break;
        switch(c) {
            case KEY_DOWN:
                move_dir = DOWN;
                break;
            case KEY_UP:
                move_dir = UP;
                break;
            case KEY_LEFT:
                move_dir = LEFT;
                break;
            case KEY_RIGHT:
                move_dir = RIGHT;
                break;
            case 'g':
                is_growing = 1;
                break;
            default:;
        }
        switch(move_dir) {
            case DOWN:
                y++;
                snake_y[ (snake_head+1) % N ] = snake_y[ snake_head ] + 1;
                snake_x[ (snake_head+1) % N ] = snake_x[ snake_head ] + 0;
                break;
            case UP:
                y--;
                snake_y[ (snake_head+1) % N ] = (snake_y[ snake_head ] - 1 + N) % N;
                snake_x[ (snake_head+1) % N ] = snake_x[ snake_head ] + 0;
                break;
            case LEFT:
                x--;
                snake_y[ (snake_head+1) % N ] = snake_y[ snake_head ];
                snake_x[ (snake_head+1) % N ] = (snake_x[ snake_head ] - 1 + N) % N;
                break;
            case RIGHT:
                x++;
                snake_y[ (snake_head+1) % N ] = snake_y[ snake_head ] + 0;
                snake_x[ (snake_head+1) % N ] = snake_x[ snake_head ] + 1;
                break;
        }
        if (!is_growing) {
            snake_tail++;
            move( snake_y[snake_tail] , snake_x[snake_tail] );
            addch(' ');
        }
        snake_head++;
        move( snake_y[snake_head] , snake_x[snake_head] );
        addch('*');
        refresh();
        napms(200);
    }
    nodelay(stdscr, FALSE);
    endwin();
    return 0;
}

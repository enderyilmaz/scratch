#include <iostream>
#include <iterator>
#include <algorithm>
#include <vector>
#include <array>
#include <typeinfo>
#include <type_traits>

using namespace std;

template <size_t N, size_t Mod>
class mod_enumerator {
    array<int, N+1> data;
public:
    mod_enumerator() : data {0} {}
    mod_enumerator(initializer_list<int> ilist) :data(ilist) {}
    mod_enumerator &operator++() {
        for (int i=0; i<N+1;i++) {
            if (data[i] == Mod-1) {
                data[i] = 0;
            } else {
                data[i]++;
                return *this;
            }
        }
        return *this;
    }
    bool is_end() { return data[N]==1; }
    void reset() { 
        for (int i=0;i<N+1;i++)
            data[i] = 0;
    }
    // ++std::rbegin(data); skip data[N]: carry
    typename array<int, N>::reverse_iterator begin() { return std::rbegin(data); }
    typename array<int, N>::reverse_iterator end()   { return ++std::rend(data)-1; }
    friend ostream &operator<<(ostream &os, mod_enumerator<N, Mod> &ma) {
        copy( std::rbegin(ma.data), ++std::rend(ma.data), ostream_iterator<int> (os, " "));
        return os;
    }
};

template <typename Iter_Dict, typename Iter_Idx, typename Iter_Dest>
void copy_indexed(  Iter_Dict begin_dict, Iter_Dict end_dict,
                    Iter_Idx  begin_idx , Iter_Idx  end_idx,
                    Iter_Dest begin_dest, Iter_Dest end_dest) {
    using category = typename iterator_traits<Iter_Dict>::iterator_category;
    if (is_same<category, random_access_iterator_tag>()) { // if iterator_type==random_access
        for (;begin_dest!=end_dest;++begin_dest, ++begin_idx) {
            *begin_dest = begin_dict[ *begin_idx ];
        }
    } else {
        for (;begin_dest!=end_dest;++begin_dest, ++begin_idx) {
            Iter_Dict it_dict = begin_dict;
            for(int idx=0;idx<*begin_idx;idx++)
                ++it_dict;
            *begin_dest = *it_dict;
        }
    }
}

int main() {
    int a[] = {1,2,3,4};
//    vector<int> a {1,2,3};
    int b[3];

    mod_enumerator<3,2> ma;

    for (;!ma.is_end();++ma) {
        copy_indexed( begin(a), end(a), begin(ma), end(ma), begin(b), end(b));
        copy( begin(b), end(b), ostream_iterator<int> (cout, " "));
        cout << endl;
    }

    return 0;
}

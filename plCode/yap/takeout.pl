#!/usr/bin/yap -L --

takeout(X,[X|T],T).
takeout(X,[Y|T],[Y|R]):-takeout(X,T,R).

perm([X],[X]).
perm([H|T],Perm):-perm(T, Perm1), takeout(H,Perm,Perm1).

:-initialization(main2).

main:-takeout(X, [1,2,3,4,5], R), write(R), nl, fail.
main2:-perm([1,2,3,4,5], R), write(R), nl, fail.

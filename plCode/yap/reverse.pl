
reverse([],[]).
reverse([H|T], [R|H]):-reverse(T,R).

:- reverse([1,2,3,4,5], R), write(R), nl.

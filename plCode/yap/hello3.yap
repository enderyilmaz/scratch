#!/usr/bin/yap -L --

main([]).
main([H|T]):-
    write(H), nl,
    main(T).

:- unix(argv(AllArgs)), main(AllArgs).

% reverse a list

using_module(library(lists)).
list_rev([],[]).
list_rev([H|T], RR):- list_rev(T, R), append(R,[H],RR).

list_rev2(X,Z):-list_rev2_(X,[],Z).
list_rev2_([],Z,Z).
list_rev2_([H|T], L1, Z):- list_rev2_(T,[H|L1],Z).

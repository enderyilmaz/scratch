<<<<<<< HEAD
% Author:
% Date: 4/23/2012

pi(3.14159265).

%init_erf_1(P, A1, A2, A3, A4, A5).
init_erf_1(0.3275911, 0.254829592, -0.284496736, 1.421413741, -1.453152027, 1.061405429).

erf_1(X,Y):- init_erf_1(P,A1,A2,A3,A4,A5), T = 1/(1+P*X),
    Y is 1-(A1*T+A2*T**2+A3*T**3+A4*T**4+A5*T**5)*exp(-X**2).
cum_norm(X,Y):- erf_1(X/sqrt(2),Y1), Y is (Y1+1)/2.
inv_erf(X,Y):-pi(Pi),A=0.140012, C is log(1-X**2),
     T1 is (2/(Pi*A) + C/2), T2 is C/A, T3 is 2/(Pi*A)+C/2,
     Y is sign(X)*sqrt(sqrt(T1**2-T2)-T3).
inv_cum_norm(X,Y):- inv_erf(2*X-1,Y1), Y is sqrt(2)*Y1.
randn(X):- random(U), inv_cum_norm(U,X).

randn_list(0, []):-!.
randn_list(N, [H|T]):- randn(H), N1 is N-1, randn_list(N1, T).
=======
% Author:
% Date: 4/23/2012

pi(3.14159265).

%init_erf_1(P, A1, A2, A3, A4, A5).
init_erf_1(0.3275911, 0.254829592, -0.284496736, 1.421413741, -1.453152027, 1.061405429).

erf_1(X,Y):- init_erf_1(P,A1,A2,A3,A4,A5), T = 1/(1+P*X),
    Y is 1-(A1*T+A2*T**2+A3*T**3+A4*T**4+A5*T**5)*exp(-X**2).
cum_norm(X,Y):- erf_1(X/sqrt(2),Y1), Y is (Y1+1)/2.
inv_erf(X,Y):-pi(Pi),A=0.140012, C is log(1-X**2),
     T1 is (2/(Pi*A) + C/2), T2 is C/A, T3 is 2/(Pi*A)+C/2,
     Y is sign(X)*sqrt(sqrt(T1**2-T2)-T3).
inv_cum_norm(X,Y):- inv_erf(2*X-1,Y1), Y is sqrt(2)*Y1.
randn(X):- random(U), inv_cum_norm(U,X).

randn_list(0, []):-!.
randn_list(N, [H|T]):- randn(H), N1 is N-1, randn_list(N1, T).
>>>>>>> 15e613dd70a4643ad5a3828de90d86af183cfd5f

% Author:
% Date: 3/28/2012

fileRead :- 
%         working_directory(CWD, '.'),
         open('data.txt', read, In),
         readAllChars(In),
         write('closing file'),
         close(In).
%         working_directory(CWD_old,CWD).
%readLine(Stream, Line) :- get_char(Stream, Char).

readAllChars(Stream):-get_char(Stream, Char)
                                       , \+ (Char == end_of_file)
                                       , !
                                       , write(Char),nl
                                       , readAllChars(Stream).

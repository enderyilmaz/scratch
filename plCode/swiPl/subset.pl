isIn(X, L):-member(X,L).
%remitem([],R,R).
remitem(H, [H|T], T).
remitem(I, [H|T], [H|R]):-remitem(I, T, R).
subset1([], _):-true.
subset1([H|T], L):-
    member(H,L),
    remitem(H,L,LR), 
    subset1(T,LR).

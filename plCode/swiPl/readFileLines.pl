% read file line by line
readFile(File):-
    open(File, read, SIN),
    read_lines(SIN, Lines),
    write(Lines),
    close(SIN).

read_lines(SIN, []):-
    at_end_of_stream(SIN).
read_lines(SIN, [Line|LINES]):-
    \+ at_end_of_stream(SIN),
    read(SIN,Line),
    read_lines(SIN, LINES).

:-readFile('data_dot.txt').

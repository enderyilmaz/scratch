% read file 1: see/seing/...

readFileProc(end_of_file):-!.
readFileProc(Line):-
    write(Line), 
    nl,
    fail.

readFileLines(File):-
    seeing(OrgStream),
    see(File),
    repeat,
    read(Line),
    readFileProc(Line),
    seen,
    see(OrgStream).

:-readFileLines('data_dot.txt').

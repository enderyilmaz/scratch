
% f(n) = n/2  if n==0 (mod 2)
% f(n) = 3n+1 if n==1 (mod 2)

p3n1(1, 0):-!.
p3n1(Num, N):-
    NumM is Num mod 2,
    (NumM == 0 ->
        NumNext is Num/2,
        p3n1(NumNext, N0),
        N is N0+1
        ;
        NumNext is 3*Num+1,
        p3n1(NumNext, N0),
        N is N0+1).

test1:-
    between(1,100,X),
    p3n1(X, N),
    write(X), write(' '),
    write(N), nl,
    fail.

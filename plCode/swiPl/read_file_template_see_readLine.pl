% read file 1: see/seing/...

readFileProc(end_of_file):-!.
readFileProc(Line):-
    write(Line), 
    nl,
    fail.

readFileLines(File):-
    seeing(OrgStream),
    see(File),
    current_input(Stream),
    repeat,
    read_line_to_string(Stream, Line),
    readFileProc(Line),
    seen,
    see(OrgStream).

readFileLines2(File):-
    seeing(OrgStream),
    see(File),
    current_input(Stream),
    repeat,
    read_line_to_string(Stream, Line),
    write(Line), nl,
    Line == end_of_file,
    seen,
    see(OrgStream).

:-readFileLines('data.txt').

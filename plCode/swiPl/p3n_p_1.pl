% f(n) = n/2  if n==0 (mod 2)
% f(n) = 3n+1 if n==1 (mod 2)

p3n1(1, 0):-!.
p3n1(Num, N):-
    NumM is Num mod 2,
    (NumM == 0 ->
        NumNext is Num/2,
        p3n1(NumNext, N0),
        N is N0+1
        ;
        NumNext is 3*Num+1,
        p3n1(NumNext, N0),
        N is N0+1).

file_lines(File, Lines) :-
    setup_call_cleanup(open(File, read, In),
       stream_lines(In, Lines),
       close(In)).

stream_lines(In, Lines) :-
    read_string(In, _, Str),
    split_string(Str, "\n", "", Lines).

process([], 0).
process([H | T], Max):-
    write(H),nl,
    split_string(H," "," ",[A,B]),
    [A|B] = H,
    number_string(N1, A),
    number_string(N2, B),
    findall(Y, (between(N1,N2, X), p3n1(X, Y)), ALL),
    max_list(ALL, Max1),
    process(T, Max2),
    Max is max(Max1, Max2).

p3n1_top(File):-
    file_lines(File, Lines),
    process(Lines, Max).

:-p3n1_top('p3n_p_1.pl').

% Author:
% Date: 3/30/2012

% two stage ota
device(m,m1,ns,nb,gnd,gnd,nfet,wb,lb).
device(m,m2,nd1,ninn,ns,ns,nfet,w1,l1).
device(m,m3,nd2,ninp,ns,ns,nfet,w1,l1).
device(m,m4,nd1,nd1,vdd,vdd,pfet,wm,lm).
device(m,m5,nd2,nd1,vdd,vdd,pfet,wm,lm).
device(m,m6,nout,nd2,vdd,vdd,pfet,w6,l6).
device(m,m7,nout,nb,gnd,gns,nfet,w7,l7).
device(r,rL, nout, gnd, r1).

diff_pair(Id1, Id2):-device(m,Id1,D1,G1,S,S,Model,W,L),
               device(m,Id2,D2,G2,S,S,Model,W,L),
               \+ (Id1==Id2),
               \+ (G1==G2),!.
%               write('diff pair:'), write(Id1),
%               write('-'), write(Id2),nl.
cs_mirror(Id1,Id2):-device(m,Id1,D1,G1,S,S,Model,_,_),
               device(m,Id2,D2,G1,S,S,Model,_,_),
               \+ (Id1==Id2).

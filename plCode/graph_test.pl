% graph algorithm test routines

gen_nodes(N,L):-
    findall( X, ( between(1,N,X)), L).
gen_node_pairs_weight(N,X,Y,W):-
    between(1,N,X),
    between(1,N,Y),
    X < Y,
    random_between(1,10,W).

graph_gen_complete_wWeights(N,L):-
    findall(
        [X,Y,W],
        gen_node_pairs_weight(N,X,Y,W),
        L
    ).

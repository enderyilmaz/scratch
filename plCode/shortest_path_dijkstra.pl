% Disjkstra's algorithm

% unweighted
%p(a,b).
%p(a,c).
%p(b,c).
%p(b,d).
%p(b,e).
%p(e,f).

:- dynamic(p/3).

p(a,b,1).
p(a,c,3).
p(a,d,5).
p(b,c,1).
p(b,d,4).
p(c,d,1).

% bidirectional exhaustivepath generation
% single source, single dest
% input:
%   1/X+: start node
%   2/Y+: dest node
%   3/T+: [X], visited aux list
%   4/- : path
path(X,Y,T,[Y|T]):-
    (p(X,Y,_);p(Y,X,_)).
path(X,Y,T,TT):- (
            p(X,Z,_),\+ member(Z,T);
            p(Z,X,_),\+ member(X,T)
    ),
    path(Z,Y,[Z|T],TT).

p2p_dist(X,Y,W):-p(X,Y,W).
p2p_dist(X,Y,W):-p(Y,X,W).

path_cost([_],0).
path_cost([X,Y|T], SUM):-
    p2p_dist(X,Y,W1),
    path_cost([Y|T], SUM0),
    SUM is SUM0+W1.

unique([],[]).
unique([X],[X]):-!.
unique([X,X|T],L):-!,
    unique([X|T], L).
unique([X,Y|T],[X|L]):-
    unique([Y|T],L).

get_all_nodes(L):-
    findall(X,p(_,X,_),L1),
    findall(Y,p(Y,_,_),L2),
    union(L1,L2,L3),
    unique(L3,L).
init_nodes([]).
init_nodes([H|T]):-
    retractall(d(H,_)),
    asserta(d(H,10)),
    init_nodes(T).
pp(X,Y,N):-p(X,Y,N).
pp(X,Y,N):-p(Y,X,N).

dijkstra_iter([]):-!.
dijkstra_iter(Lnode):-
    findall(
        D_,
        ( member(X,Lnode), d(X,D_)
        ),
        DistList),
    min_member(MinDist, DistList),
    d(Nmin, MinDist), 
    select(Nmin,Lnode,Lnode0),
    forall(
        (
            pp(Nmin,X,Dist), 
            member(X,Lnode0)
        ),
        (
            d(X,DistX),
            NewDist is Dist+MinDist,
            (NewDist<DistX->
                retract(d(X,_)),
                asserta(d(X,NewDist))
                ; true
            )
        )
    ),
    dijkstra_iter(Lnode0).

dijkstra(Src,Dst,ShortestDistance):-
    get_all_nodes(Lnode),
    init_nodes(Lnode),
    retract(d(Src,_)),
    asserta(d(Src,0)),
    dijkstra_iter(Lnode),
    d(Dst,ShortestDistance).

['graph_test.pl'].

gen_complete_graph(N):-
    graph_gen_complete_wWeights(N,L),
    retractall(p(_,_,_)),
    forall(
        member([X,Y,W], L),
        asserta(p(X,Y,W))
        ).
test_dijkstra:-
    N is 1000,
    gen_complete_graph(N),
    time(dijkstra(1,N,ShortestDist)),
    write(ShortestDist).

<<<<<<< HEAD
% Author:
% Date: 7/1/2012

plot_list(XL,YL,Box):-use_module(library(plot/plotter)),
      use_module(library(autowin)),
      [Xmin,Xmax,Ymin,Ymax] = Box,
      Step is (Xmax-Xmin)/5,
      new(W, auto_sized_picture('Plotter demo')),
      send(W, display, new(P, plotter)),
      send(P, axis, new(X, plot_axis(x, Xmin, Xmax, Step, 300))),
      send(P, axis, plot_axis(y, Ymin, Ymax, @default, 200)),
      send(X, format, '%.2f'),
      send(P, graph, new(G, plot_graph)),
      plot_function(G,XL,YL),
      send(W, open).

plot_function(_,[],_).
plot_function(_,_,[]).
=======
% Author:
% Date: 7/1/2012

plot_list(XL,YL,Box):-use_module(library(plot/plotter)),
      use_module(library(autowin)),
      [Xmin,Xmax,Ymin,Ymax] = Box,
      Step is (Xmax-Xmin)/5,
      new(W, auto_sized_picture('Plotter demo')),
      send(W, display, new(P, plotter)),
      send(P, axis, new(X, plot_axis(x, Xmin, Xmax, Step, 300))),
      send(P, axis, plot_axis(y, Ymin, Ymax, @default, 200)),
      send(X, format, '%.2f'),
      send(P, graph, new(G, plot_graph)),
      plot_function(G,XL,YL),
      send(W, open).

plot_function(_,[],_).
plot_function(_,_,[]).
>>>>>>> 15e613dd70a4643ad5a3828de90d86af183cfd5f
plot_function(G,[X|XL],[Y|YL]):-send(G, append, X, Y), plot_function(G,XL,YL).
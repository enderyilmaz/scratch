<<<<<<< HEAD
% Author:
% Date: 4/23/2012

calcBerBPSK([],[],[]):-!.
calcBerBPSK([SNR|SNRT],[BER|BERT],[N|NT]):-calcBerBPSK_(SNR, 100, 0, 0, BER, N), calcBerBPSK(SNRT,BERT,NT).
calcBerBPSK_(_,NMaxErr, NErr, NBit, BER,NBit):-NMaxErr = NErr, BER is NErr/NBit, !.
calcBerBPSK_(SNR,NMaxErr, NErr, NBit, BER, N):-randn(Noise), random(0,1,Bit),
                          mapBitBPSK(Bit, BPSK_out), BPSK_out_scaled is sqrt(SNR*2)*BPSK_out, %% SNR*2 !!!
                          Out is BPSK_out_scaled + Noise,
                          mapBPSKBit(Out, Bit_get),
                          (Bit\=Bit_get->NErr1 is NErr + 1; NErr1 is NErr),
                          NBit1 is NBit + 1,
                          calcBerBPSK_(SNR, NMaxErr, NErr1, NBit1, BER,N).
calcBerQPSK([],[],[]):-!.
calcBerQPSK([SNR|SNRT],[BER|BERT],[N|NT]):-calcBerQPSK_(SNR, 100, 0, 0, BER, N), calcBerQPSK(SNRT,BERT,NT).
calcBerQPSK_(_,NMaxErr, NErr, NBit, BER,NBit):-  NMaxErr =< NErr, BER is NErr/NBit, !.
calcBerQPSK_(SNR,NMaxErr, NErr, NBit, BER, N):-randn(Noise1), randn(Noise2),
                          random(0,1,Bit1), random(0,1,Bit2),
                          mapBitQPSK([Bit1,Bit2], [QPSK_I,QPSK_Q]),
                          QPSK_I_scaled is sqrt(SNR*2)*QPSK_I, %% SNR*2 !!!
                          QPSK_Q_scaled is sqrt(SNR*2)*QPSK_Q, %% SNR*2 !!!
                          Out_I is QPSK_I_scaled + Noise1,
                          Out_Q is QPSK_Q_scaled + Noise2,
                          mapQPSKBit([Out_I,Out_Q], [Bit1_get, Bit2_get]),
                          (Bit1\=Bit1_get->NErr1 is NErr + 1; NErr1 is NErr),
                          (Bit2\=Bit2_get->NErr2 is NErr1 + 1; NErr2 is NErr1),
                          NBit1 is NBit + 2,
                          calcBerQPSK_(SNR, NMaxErr, NErr2, NBit1, BER,N).

loadlib:-consult('C:/Users/eyilmaz2/Dropbox/plCode/qamModLib.pl'),
     consult('C:/Users/eyilmaz2/Dropbox/plCode/randLib.pl'),
     consult('C:/Users/eyilmaz2/Dropbox/plCode/statLib.pl').
run:-loadlib,
     calcBerQPSK([1,2,3,4,5,6],BER1,_), maplist(writeln,BER1),
     calcBerBPSK([1,2,3,4,5,6],BER2,N), maplist(writeln,BER2).
run1:-calcBerQPSK([1,2,3,4,5,6],BER1,_), maplist(writeln,BER1),
     calcBerBPSK([1,2,3,4,5,6],BER2,N), maplist(writeln,BER2).
map











=======
% Author:
% Date: 4/23/2012

calcBerBPSK([],[],[]):-!.
calcBerBPSK([SNR|SNRT],[BER|BERT],[N|NT]):-calcBerBPSK_(SNR, 100, 0, 0, BER, N), calcBerBPSK(SNRT,BERT,NT).
calcBerBPSK_(_,NMaxErr, NErr, NBit, BER,NBit):-NMaxErr = NErr, BER is NErr/NBit, !.
calcBerBPSK_(SNR,NMaxErr, NErr, NBit, BER, N):-randn(Noise), random(0,1,Bit),
                          mapBitBPSK(Bit, BPSK_out), BPSK_out_scaled is sqrt(SNR*2)*BPSK_out, %% SNR*2 !!!
                          Out is BPSK_out_scaled + Noise,
                          mapBPSKBit(Out, Bit_get),
                          (Bit\=Bit_get->NErr1 is NErr + 1; NErr1 is NErr),
                          NBit1 is NBit + 1,
                          calcBerBPSK_(SNR, NMaxErr, NErr1, NBit1, BER,N).
calcBerQPSK([],[],[]):-!.
calcBerQPSK([SNR|SNRT],[BER|BERT],[N|NT]):-calcBerQPSK_(SNR, 100, 0, 0, BER, N), calcBerQPSK(SNRT,BERT,NT).
calcBerQPSK_(_,NMaxErr, NErr, NBit, BER,NBit):-  NMaxErr =< NErr, BER is NErr/NBit, !.
calcBerQPSK_(SNR,NMaxErr, NErr, NBit, BER, N):-randn(Noise1), randn(Noise2),
                          random(0,1,Bit1), random(0,1,Bit2),
                          mapBitQPSK([Bit1,Bit2], [QPSK_I,QPSK_Q]),
                          QPSK_I_scaled is sqrt(SNR*2)*QPSK_I, %% SNR*2 !!!
                          QPSK_Q_scaled is sqrt(SNR*2)*QPSK_Q, %% SNR*2 !!!
                          Out_I is QPSK_I_scaled + Noise1,
                          Out_Q is QPSK_Q_scaled + Noise2,
                          mapQPSKBit([Out_I,Out_Q], [Bit1_get, Bit2_get]),
                          (Bit1\=Bit1_get->NErr1 is NErr + 1; NErr1 is NErr),
                          (Bit2\=Bit2_get->NErr2 is NErr1 + 1; NErr2 is NErr1),
                          NBit1 is NBit + 2,
                          calcBerQPSK_(SNR, NMaxErr, NErr2, NBit1, BER,N).

loadlib:-consult('C:/Users/eyilmaz2/Dropbox/plCode/qamModLib.pl'),
     consult('C:/Users/eyilmaz2/Dropbox/plCode/randLib.pl'),
     consult('C:/Users/eyilmaz2/Dropbox/plCode/statLib.pl').
run:-loadlib,
     calcBerQPSK([1,2,3,4,5,6],BER1,_), maplist(writeln,BER1),
     calcBerBPSK([1,2,3,4,5,6],BER2,N), maplist(writeln,BER2).
run1:-calcBerQPSK([1,2,3,4,5,6],BER1,_), maplist(writeln,BER1),
     calcBerBPSK([1,2,3,4,5,6],BER2,N), maplist(writeln,BER2).
map











>>>>>>> 15e613dd70a4643ad5a3828de90d86af183cfd5f

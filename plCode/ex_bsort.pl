% bubble sort

bsort_iter([H], [H]):-!.
bsort_iter([H1,H2|T],[H2|L0]):-
    H1>H2,!,
    bsort_iter( [H1|T], L0 ).
bsort_iter([H1,H2|T],[H1|L0]):-
    bsort_iter( [H2|T], L0 ).

is_sorted([_]):-!.
is_sorted([H1,H2|T]):-
    H1=<H2,!,
    is_sorted([H2|T]).

bubble_sort1(LI,LO):-
    length(LI,N), N1 is N-1,
    bubble_sort1_(LI,N1,LO). 
bubble_sort1_(LI,0,LI):-!.
bubble_sort1_(LI,N,LO):-
    N1 is N-1,
    bsort_iter(LI,L_),
    bubble_sort1_(L_,N1,LO).

bubble_sort2(LI,LI):-
    is_sorted(LI),!.
bubble_sort2(LI,LO):-
    bsort_iter(LI,L_),
    bubble_sort2(L_, LO).

test_aux_gen_random_list(N,L):-
    findall(X, (between(1,N,_), random_between(1,N,X)), L).

test_bsort_size(10000).
test_bsort1:-
    test_bsort_size(N),
    test_aux_gen_random_list(N,LI),
    time(bubble_sort1(LI,_)).
test_bsort2:-
    test_bsort_size(N),
    test_aux_gen_random_list(N,LI),
    time(bubble_sort2(LI,_)).

#!/usr/bin/env perl
use strict;
use warnings;
use Data::Dumper;

sub print_array {
    print "$_ " for (@_);
    print "\n";
}
# working
sub permutations_rec {
    my ($func_p, $free_list_p, @used_list) = @_;
    my @free_list = @$free_list_p;
    if (@free_list) {
        for my $i (0..$#free_list) {
            my $v = shift @free_list;
            permutations_rec($func_p, \@free_list, $v, @used_list);  
            push @free_list, $v;
        }
    } else {
        &$func_p(@used_list);
    }
}
# not working !!!
sub permutations_loop {
    my @free_list = @_;
    my $max_depth = $#free_list;
    my $cur_pos = 0;
    my @hist_path = ( \@free_list );
    my @path = ();
    while($cur_pos>=0) {
        if ($max_depth+1 == $cur_pos ) {
            print "$_ " for (@path);
            $cur_pos -= 1;
            pop @hist_path;
            next;
        }
        if ( @{$hist_path[$cur_pos]} ) {
            my @p = @{$hist_path[$cur_pos]};
            shift @{$hist_path[$cur_pos]};
            my $v = shift @p;
            push @path, $v;
            my @new_node = @p;
            push @hist_path, \@new_node;
            $cur_pos += 1;
        } else {
            pop @hist_path;
            pop @path;
            $cur_pos -= 1;
        }
    }
}
sub permutation_next {
    my $lst_p = shift;
    my @lst = @$lst_p;
    my $i=$#lst-1;
    while($lst_p->[$i]>$lst_p->[$i+1]) {
        $i -= 1;
        return 0 if $i==-1;
    }
    my $j = $#lst;
    while ($lst_p->[$j] < $lst_p->[$i] && $j>$i) {
        $j -= 1;
    }
    @$lst_p[$i,$j] = @$lst_p[$j,$i];

    my $left = $i+1;
    my $right=$#lst;
    while($left < $right) {
        @$lst_p[$left, $right] = @$lst_p[$right, $left];
        $left++;
        $right--;
    }
#    print Dumper($lst_p);
    return 1;
}

my @free_list = 0..5;
my @used_list = ();

#permutations_rec(\&print_array, \@free_list, @used_list);

#permutations_loop( 0..2);
#

print "$_ " for (@free_list); print "\n";
while( permutation_next( \@free_list) ) {
    print "$_ " for (@free_list);
    print "\n";
}

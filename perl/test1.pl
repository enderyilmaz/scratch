#!/usr/bin/perl

use strict;
use warnings;

print("hello!\n");

my @list1 = (2,3,4,8,9);
my @list2 = qw/ender yilmaz c++ asm/;

for (@list1) {
	print("$_\n");
}
for (@list2) {
	print("$_\n");
}

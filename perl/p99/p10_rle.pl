#!/usr/bin/perl

use strict;
use warnings;

my @lst = (2,2,4,3,3,3,5,5);

sub rle {
    my $last = -1;
    my $cnt  = 0;
    my @retlist = ();
    while (@_) {
        if ($last eq -1) {
            $last = $_;
            $cnt = 1;
        } elsif ($_ == $last) {
            $cnt++;
        } else {
            push(@retlist, $cnt, $last);
            $last = $_;
            $cnt = 1;
        }
    }
    if ($cnt>0) {
        push(@retlist, $cnt, $last);
    }
    return @retlist;
}

my @lst2;

while(@lst) {
    print "$_\n";
}
return;

@lst2 = rle( @lst );

print( join( ' ', @lst2 ) );


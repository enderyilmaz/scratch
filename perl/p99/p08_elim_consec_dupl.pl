#!/usr/bin/perl

use warnings;
use strict;

sub elim_cons_dup {
    my @out = ();
    my $last;
    foreach (@_) {
        if (defined $last && $last == $_) {
            next;
        } else {
            push @out, $_;
        }
        $last = $_;
    }
    return @out;
}

my @lst = (1, 2,2,3,1,1,3,3);

print ( join (',' , @lst ) . "\n");
print ( join (',' , elim_cons_dup(@lst) ) . "\n");

#!/usr/bin/perl

use warnings;
use strict;
use Data::Dumper;

#my @lst = [1,2,4, [5,6]];
my @lst = (1,2,4, [5,6]);

sub flatten {
    my $ptr_list = shift @_;
    my @flat = ();
    for my $item (@$ptr_list) {
        if (ref $item) {
            push @flat, flatten($item);
        } else {
            push @flat, $item
        }
    }
    return @flat;
}

sub flatten2 { map { ref $_ ? flatten2(@$_) : $_ } @_ }

print Dumper(@lst);
print Dumper( flatten2(\@lst) );

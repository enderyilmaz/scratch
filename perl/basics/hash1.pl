#!/usr/bin/perl

my %hash1;
$hash1{'Mango'} = 10;
$hash1{'Apple'} = 20;
$hash1{'Kiwi'}  = 30;

my %hash2 = ('Mango' => 45, 'Apple' => 42, 'Kiwi' => 35);

my @key_list = keys %hash1;

for (keys %hash1) {
    print("$_ => $hash1{$_}\n");
}

#!/usr/bin/perl

use warnings;
use strict;

sub func {
    my ($fst, @rest) = @_;
    print $fst;
    print join(',', @rest);
}

func("ender","yilmaz",1,2,3,4,"\n")

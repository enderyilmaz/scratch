# Opening the file 
open(fh, "GFG2.txt") or die "File '$filename' can't be opened"; 

# Reading First line from the file 
$firstline = <fh>; 
print "$firstline\n"; 

# Opening file Hello.txt in write mode 
open (fh, ">", "Hello.txt"); 

# Getting the string to be written 
# to the file from the user 
print "Enter the content to be added\n"; 
$a = <>; 

# Writing to the file 
print fh $a; 

# Closing the file 
close(fh) or "Couldn't close the file"; 

# Using predefined modules 
use warnings; 
use strict; 

# Providing path of file to a variable 
my $filename = 'C:\Users\GeeksForGeeks\GFG.txt'; 

# Checking for the file existence 
if(-e $filename) 
{ 

    # If File exists 
    print("File $filename exists\n"); 
} 

else
{ 

    # If File doesn't exists 
    print("File $filename does not exists\n"); 
} 


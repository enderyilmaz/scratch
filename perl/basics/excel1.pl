#!/usr/bin/perl 
### Write
use Excel::Writer::XLSX; 

my $Excelbook = Excel::Writer::XLSX->new( 'GFG_Sample.xlsx' ); 
my $Excelsheet = $Excelbook->add_worksheet(); 

$Excelsheet->write( "A1", "Hello!" ); 
$Excelsheet->write( "A2", "GeeksForGeeks" ); 
$Excelsheet->write( "B1", "Next_Column" ); 

$Excelbook->close; 

### Read
use 5.016; 
use Spreadsheet::Read qw(ReadData); 
my $book_data = ReadData (‘new_excel.xlsx'); 
say 'A2: ' . $book_data->[1]{A2}; 


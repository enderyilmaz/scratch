#!/usr/bin/perl

use warnings;
use strict;

sub llist_create { return [] }

sub llist_add {
    my $ptr_list = shift;
    my $item = shift;
    my $new_ptr_list = [$item, $ptr_list];
    return $new_ptr_list;
}

sub llist_toStr {
    my $ptr_list = shift;
    my @lst = ();
    while (@$ptr_list) {
        push @lst, $ptr_list->[0];
        $ptr_list = $ptr_list->[1];
    }
    join(qq/,/, reverse @lst);
}

my $ll = llist_create;
$ll = llist_add($ll, 1);
$ll = llist_add($ll, 2);
$ll = llist_add($ll, 3);
my $str_ll = llist_toStr($ll);

print(">>$str_ll\n");

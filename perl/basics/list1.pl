#!/usr/bin/perl

use strict;
use warnings;

my @list1 = (1, 2, 3, 4, 5);
my @list2 = qw/1 2 3 4 5/;
my @list3 = (1..5);

for (@list1) {
    print("a $_\n");
}

for (my $i=0;$i<=$#list2;$i++) {
    print("b $list2[$i]\n");
}
for (my $i=0;$i<scalar @list3;$i++) {
    print("b2 $list2[$i]\n");
}

foreach my $item (@list3) {
    print("c $item\n");
}

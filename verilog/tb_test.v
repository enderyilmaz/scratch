module tb;

reg A, clk;

always #10 clk = ~clk;

initial
begin
    $dumpfile("test.vcd");
    $dumpvars(0,tb);

    clk = 'b0;
    A = 'd0;

    $display("hello");

    @ (negedge clk);
    A = 'd5;
    @ (negedge clk);
    A = 'd0;

    #100;
    $finish;
end

initial begin
    repeat (20) @(negedge clk);
    $finish;
end

endmodule

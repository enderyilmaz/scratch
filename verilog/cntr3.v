module cntr3(mx_cnt,arstb,clk,en,cnt,tick);
input [2:0] mx_cnt;
input arstb,clk,en;

output reg [2:0] cnt=0;
output wire tick;

reg [2:0] cnt_next;
wire [2:0] cnt_next_p;

reg c;

initial begin
    cnt = 'b000;
end

always @(posedge clk or negedge arstb) begin
    if (!arstb)
        cnt = 3'd0;
    else begin
        cnt <= cnt_next;
    end
end

assign tick = cnt_next_p == mx_cnt;
assign cnt_next_p = cnt+1;

always a (*) begin
    cnt_next = (tick) ? 3'd0 : cnt_next_p;
end

endmodule

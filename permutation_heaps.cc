#include <iostream>
#include <algorithm>
#include <iterator>

using namespace std;

template <typename T, size_t N>
void next_perm_heap_rec(int n, T (&arr)[N]) {
    if (n==1) {
        copy(begin(arr), end(arr), ostream_iterator<int> (cout, ", "));
        cout << endl;
    } else {
        for (int i=0;i<n-1;i++){
            next_perm_heap_rec(n-1, arr);
            if (n%2)// odd
                swap(arr[0], arr[n-1]);
            else    // even
                swap(arr[i], arr[n-1]);
        }
        next_perm_heap_rec(n-1, arr);
    }
}

template <typename T, size_t N>
void next_perm_heap_loop(int n, T (&arr)[N]) {
    int c[N] {0};

    copy(begin(arr), end(arr), ostream_iterator<int> (cout, ", "));
    cout << endl;

    int i=0;
    while (i<n) {
        if (c[i]<i) {
            if (i%2) // odd
                swap(arr[c[i]], arr[i]);
            else // even
                swap(arr[0], arr[i]);
                copy(begin(arr), end(arr), ostream_iterator<int> (cout, ", "));
                cout << endl;
                c[i]++;
                i=0;
        } else {
            c[i] = 0;
            i++;
        }

    }
}

int main() {
    int a[] = {1,2,3,4};
    next_perm_heap_loop(4, a);
    return 0;
}

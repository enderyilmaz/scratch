
def str_match_len(str1, str2):
    sz = 0
    for a,b in zip(str1, str2):
        if a==b:
            sz = sz + 1
        else:
            break
    return sz

def gen_all_substrings(str1):
    for start_idx in range(len(str1)-1):
        for end_idx in range(start_idx+1, len(str1)+1):
            yield str1[start_idx:end_idx]

def insert_rec(root,str1, idx):
    inserted = False
    for node in root[2:-1]: # blindly insert to trie nodes
        inserted = insert_rec_(node, str1, idx)
        if inserted:
            break
    if not inserted: # if not trie matches, create one and insert
        new_node = [str1[0], idx]
#        new_node = [str1[0], -1]
        root.append(new_node)
        insert_rec_(new_node, str1, idx)
def insert_loop(root, str1, idx):
    cur_node = root
    while True:
        if cur_node[0] == "": # root node
            inserted = False
            for node in cur_node[2:]: # blindly insert to trie nodes
                if node[0]==str1[0]: #next node found
                    cur_node = node
                    inserted = True
                    break
            if not inserted:
                new_node = [str1[0], idx]
                cur_node.append(new_node)
                cur_node = new_node
        else: # leaf node
            inserted = False
            assert(cur_node[0] == str1[0])
            if len(str1)==1: # if last char, simply updated idx and exit
                cur_node[1] = idx
                break  # BREAK out of the INFINITE LOOP
            else: # insert the rest
                for node in cur_node[2:]: # insert the rest blindly
                    if node[0] == str1[1]:
                        cur_node = node
                        inserted = True
                        str1 = str1[1:]
                        break
                if not inserted: # if no trie match found, create & insert
                    new_node = [str1[1], idx]
                    cur_node.append(new_node)
                    cur_node = new_node
                    str1 = str1[1:]

def insert_rec_(root, str1, idx):
    inserted = False
    if root[0] == str1[0]: # first char must match. otherwise NOP
        if len(str1)==1: # if last char, simply updated idx and exit
#            assert(root[1]==-1)
            root[1] = idx
            inserted = True
        else: # insert the rest
            for node in root[2:]: # insert the rest blindly
                inserted = insert_rec_(node, str1[1:], idx)
                if inserted:
                    break
            if not inserted: # if no trie match found, create & insert
#                new_node = [str1[1], -1]
                new_node = [str1[1], idx]
                root.append(new_node)
                inserted = insert_rec_(new_node, str1[1:], idx) 
    return inserted

def dump(root, ntab):
    str1 = "".join(["    " for x in range(ntab)])
    print("%s%d,%s" % (str1, root[1], root[0]))
    for node in root[2:]:
        dump(node, ntab+1)

def sffx_trie_match_loop(root, str1):
    cur_node = root
    pos = -1
    while True:
        if cur_node[0] == '': # root
            for node in cur_node[2:]:
                if node[0] == str1[0]:
                    cur_node = node
                    break
        else:
            if len(str1)==1: # termination condition
                return cur_node[1]
            else:
                for node in cur_node[2:]:
                    if node[0] == str1[1]:
                        cur_node = node
                        str1 = str1[1:]
                        break

def sffx_trie_match_rec(root , str1):
    for node in root[2:]:
        if node[0] == str1[0]:
            pos = sffx_trie_match_rec_(node, str1[0:])
            if pos>=0:
                return pos
    return -1
def sffx_trie_match_rec_(root, str1):
    if len(root)>0 and root[0] == str1[0]:
        if len(str1)==1:
            return root[1]
        else:
            for node in root[2:]:
                if node[0] == str1[1]:
                    pos = sffx_trie_match_rec_(node, str1[1:])
                    if pos>=0:
                        return pos
    return -1

import random
import sys

if __name__=="__main__":

    txt = "NNGSGJGDPJ"
    txt = "BANANA"
#    txt = "abc"

    print(txt)

    trie = ["", -1]

#1    insert_loop(trie, txt, 0)
#1    insert_loop(trie, txt[1:], 1)
#1    insert_loop(trie, txt[2:], 2)
#1    insert_loop(trie, txt[3:], 3)
#1    insert_loop(trie, txt[4:], 4)
#1    insert_loop(trie, txt[5:], 5)

#1    for idx in range(len(txt)):
#1        insert_loop(trie, txt[idx:], idx)
#1        print(txt[idx:])
#1
#1    print(trie)
#1    dump(trie, 0)
#1
#1    print( sffx_trie_match_loop(trie, "ANA") )
#1    quit()

################################################
    str_len = 10
    sys.setrecursionlimit(str_len+100)
    txt = "".join([chr(65+random.randint(0,20)) for i in range(str_len)])

    trie = ["",-1]
    for sub_idx in range(len(txt)):
            str_ins = txt[sub_idx:]
            insert_rec(trie, str_ins, sub_idx);
            print("INS@%d: %s" % (sub_idx, str_ins));

#    print(trie)
#    dump(trie, 0)

    mnum = 0
    for substr in gen_all_substrings(txt):
        print("matching %d: %s" % (mnum, substr))
        idx = sffx_trie_match_loop(trie, substr)
        if( not txt[idx:].startswith( substr ) ):
            print(idx, txt[idx:], substr)
        assert( txt[idx:].startswith( substr ) )
        mnum = mnum + 1

#    tree = ["",-1]
#    for sub_idx in range(len(txt)):
#        str_ins = txt[sub_idx:]
#        insert(tree, txt[sub_idx:], sub_idx);
##        print(txt[sub_idx:]);
##        print(tree)
##    dump_tree(tree, 0)
##    for substr in ["GS"]: #gen_all_substrings(txt):
#    for substr in gen_all_substrings(txt):
#        idx = suffix_find(tree, substr)
#        if( not txt[idx:].startswith( substr ) ):
#            print(idx, txt[idx:], substr)
##        assert( txt[idx:].startswith( substr ) )

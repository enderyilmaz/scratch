#include <iostream>
#include <vector>
#include <array>
#include <iterator>

using namespace std;

template <typename T>
class VWrap {
    vector<T> v {1,2,3,4};
public:
    typedef typename vector<T>::iterator iter;
    VWrap() {}
    typename vector<T>::iterator begin() { return v.begin(); };
    typename vector<T>::iterator end() { return v.end(); };
};
template <typename T, size_t N>
class AWrap {
    array<T, N> arr {0};
public:
    typename array<T,N>::iterator begin() { return arr.begin(); };
    typename array<T,N>::iterator end() { return arr.end(); };
};

int main() {
    VWrap<int> vw;

    copy( vw.begin(), vw.end(), ostream_iterator<int>(cout, " "));
    cout << endl;

    AWrap<int, 4> aw;
    copy( aw.begin(), aw.end(), ostream_iterator<int>(cout, " "));
    cout << endl;
    return 0;
}

# edit distance
#keywords edit-distance global-align local-align approximate-match

import sys

def edit_distance_rec(str1, str2, n1, n2):
#    print("%s %s %d %d" % (str1, str2, n1, n2))
    if n1==0:
        return n2
    if n2==0:
        return n1

    delta = 0
    if str1[n1-1] == str2[n2-1]:
        delta = 0
    else:
        delta = 1

    v1 = edit_distance_rec(str1, str2, n1-1, n2-1)
    v2 = edit_distance_rec(str1, str2, n1-1, n2  ) + 1
    v3 = edit_distance_rec(str1, str2, n1  , n2-1) + 1
    vret = min( min(v2,v3), v1+delta)
    return vret

def edit_distance_dp_bottomup(txt, pat, n, m):
    mx = [[0]*(n+1) for _ in range(m+1)]
    for j in range(m+1):
        mx[j][0] = j
    for i in range(n+1):
        mx[0][i] = i

    for i in range(1,n+1):
        for j in range(1,m+1):
            delta = 0 if txt[i-1]==pat[j-1] else 1
            v1 = mx[j-1][i-1] + delta 
            v2 = mx[j-1][i] + 1
            v3 = mx[j][i-1] + 1
            vv = min(min(v1,v2),v3)
            mx[j][i] = vv
    vret = mx[-1][-1] # the last item is edit distance

    for j in range(m+1):
        for i in range(n+1):
            print("%3d"%(mx[j][i]), end="")
        print("")

    return vret

def global_align_db_bottomup(txt, pat, cost_matrix):
    return edit_distance_cost_matrix_dp_bottomup(txt, pat, len(txt), len(pat), cost_matrix)

def edit_distance_cost_matrix_dp_bottomup(txt, pat, n, m, cost_matrix):
    mx = [[0]*(n+1) for _ in range(m+1)]
    cum_cost = 0
    for j in range(1, m+1):
        cum_cost = cum_cost + cost_matrix[ (" ", pat[j-1]) ]
        mx[j][0] = cum_cost
    cum_cost = 0
    for i in range(1, n+1):
        cum_cost = cum_cost + cost_matrix[ (txt[i-1], " ") ]
        mx[0][i] = cum_cost

    for i in range(1,n+1):
        for j in range(1,m+1):
            v1 = mx[j-1][i-1] + cost_matrix[ (txt[i-1], pat[j-1] ) ]
            v2 = mx[j-1][i]   + cost_matrix[ (" "     , pat[j-1] ) ]
            v3 = mx[j][i-1]   + cost_matrix[ (txt[i-1], " "      ) ]
            vv = min(min(v1,v2),v3)
            mx[j][i] = vv
    vret = mx[-1][-1] # the last item is edit distance

    for j in range(m+1):
        for i in range(n+1):
            print("%3d"%(mx[j][i]), end="")
        print("")

    # Trace Back
    i = n
    j = m
    steps = []
    while j>0:
        cur = mx[j][i]
        cost_diag = cost_matrix[ (txt[i-1], pat[j-1] ) ]
        v1 = mx[j-1][i-1] + cost_diag
        v2 = mx[j-1][i]   + cost_matrix[ (" "     , pat[j-1] ) ]
        v3 = mx[j][i-1]   + cost_matrix[ (txt[i-1], " "      ) ]
        if cur == v1: # came from diagonal
            if cost_diag==0: # match
                steps.append( "Match %s" % (txt[i-1]) )
            else:
                steps.append( "Swap %s->%s" % (pat[j-1], txt[i-1]))
            j = j - 1
            i = i - 1
            continue
        if cur == v2: # came from vertical
            steps.append("Insert %s" % (pat[j-1]) )
            j = j - 1
            continue
        if cur == v3: # came from horizontal
            steps.append("Delete %s" % (txt[i-1]) )
            i = i - 1
            continue
    steps.reverse()
    print(steps)

    return vret

def local_align_db_bottomup(txt, pat, score_matrix):
    return edit_distance_score_matrix_dp_bottomup(txt, pat, len(txt), len(pat), score_matrix)
def edit_distance_score_matrix_dp_bottomup(txt, pat, n, m, score_matrix):
    mx = [[0]*(n+1) for _ in range(m+1)]

    max_val = -sys.maxsize
    max_val_ji_pair = (-1, -1)
    for i in range(1,n+1):
        for j in range(1,m+1):
            v1 = mx[j-1][i-1] + score_matrix[ (txt[i-1], pat[j-1] ) ]
            v2 = mx[j-1][i]   + score_matrix[ (" "     , pat[j-1] ) ]
            v3 = mx[j][i-1]   + score_matrix[ (txt[i-1], " "      ) ]
            vv = max(max(v1,v2),max(v3,0))
            mx[j][i] = vv
            if vv>max_val:
                max_val =vv
                max_val_ji_pair = (j,i)

    for j in range(m+1):
        for i in range(n+1):
            print("%3d"%(mx[j][i]), end="")
        print("")
    print( max_val_ji_pair )


    # Trace Back
    j,i = max_val_ji_pair
    steps = []
    while mx[j][i]>0:
        cur = mx[j][i]
        score_diag = score_matrix[ (txt[i-1], pat[j-1] ) ]
        v1 = mx[j-1][i-1] + score_diag
        v2 = mx[j-1][i]   + score_matrix[ (" "     , pat[j-1] ) ]
        v3 = mx[j][i-1]   + score_matrix[ (txt[i-1], " "      ) ]
        if cur == v1: # came from diagonal
            if score_diag>=0: # match
                steps.append( "Match %s" % (txt[i-1]) )
            else:
                steps.append( "Swap %s->%s" % (pat[j-1], txt[i-1]))
            j = j - 1
            i = i - 1
            continue
        if cur == v2: # came from vertical
            steps.append("Insert %s" % (pat[j-1]) )
            j = j - 1
            continue
        if cur == v3: # came from horizontal
            steps.append("Delete %s" % (txt[i-1]) )
            i = i - 1
            continue
        print(j,i)
        break
    steps.reverse()
    print(steps)
def edit_distance_approximate_match(txt, pat, n, m):
    """ -----TEXXXXXXXXXXXXXT--- (n) - x
       P
       A  [y,x]
       T  [j][i]
       T
       E
       R
       N(m) - y

    txt=TATTGGCTATACGGTT
    pat=GCGTATGC
    0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
    1 1 1 1 1 0 0 1 1 1 1 1 1 0 0 1 1
    2 2 2 2 2 1 1 0 1 2 2 2 1 1 1 1 2
    3 3 3 3 3 2 1 1 1 2 3 3 2 1 1 2 2
    4 3 4 3 3 3 2 2 1 2 2 3 3 2 2 1 2
    5 4 3 4 4 4 3 3 2 1 2 2 3 3 3 2 2
    6 5 4 3 4 5 4 4 3 2 1 2 3 4 4 3 2
    7 6 5 4 4 4 5 5 4 3 2 2 3 3 4 4 3
    8 7 6 5 5 5 5 5 5 4 3 3(2)3 4 5 4 <-- LAST ROW
    4                       ^
                            |= match end

    """
    mx = [[0]*(n+1) for _ in range(m+1)]
    for j in range(m+1):
        mx[j][0] = j

    for i in range(1,n+1):
        for j in range(1,m+1):
            delta = 0 if txt[i-1]==pat[j-1] else 1
            v1 = mx[j-1][i-1] + delta 
            v2 = mx[j-1][i] + 1
            v3 = mx[j][i-1] + 1
            vv = min(min(v1,v2),v3)
            mx[j][i] = vv

    # Distance matrix is mx

    for j in range(m+1):
        for i in range(n+1):
            print("%2d"%(mx[j][i]), end="")
        print("")

    # now find match edit distance = min( mx[-1][:] ): last row
    i_min = 0
    i_dist_min = sys.maxsize
    for i in range(n):
        if mx[-1][i] < i_dist_min:
            i_min = i
            i_dist_min = mx[-1][i]
    end_offset = i_min
    edit_dist = i_dist_min
    
    # Trace back
    i = i_min
    j = m
    txt_match_sequence = []
    while j>0:
        val = mx[j][i]
        delta = 0 if txt[i-1]==pat[j-1] else 1
        if mx[j-1][i-1]+delta == mx[j][i]: # came from diagonal
            i = i - 1
            j = j - 1
            if delta == 0:
                txt_match_sequence.append("Match %s" % (txt[i]))
            else:
                txt_match_sequence.append("Swap %s->%s" % (txt[i], pat[j]))
            continue
        if mx[j-1][i]+1 == mx[j][i]: # came from vertical
            j = j - 1
            txt_match_sequence.append("Insert %s" % (pat[j]))
            continue
        if mx[j][i-1]+1 == mx[j][i]: # came from horizontal
            i = i - 1
            txt_match_sequence.append("Drop %s" % (txt[i]))
            continue
    start_offset = i + 1
    txt_match_sequence.append("#@%d, D=%d" % (start_offset, edit_dist))
    txt_match_sequence.reverse()
    print(txt_match_sequence)

    # Generate Align Strings
    txt_align = []
    pat_align = []
    i = i_min
    j = m
    while j>0:
        val = mx[j][i]
        delta = 0 if txt[i-1]==pat[j-1] else 1
        if mx[j-1][i-1]+delta == mx[j][i]: # came from diagonal
            i = i - 1
            j = j - 1
            if delta == 0:
                txt_align.append( txt[i] )
                pat_align.append( pat[j] )
            else:
                txt_align.append( txt[i].lower() if txt[i].isupper() else txt[i].upper() )
                pat_align.append( pat[j].lower() if pat[j].isupper() else pat[j].upper() )
            continue
        if mx[j-1][i]+1 == mx[j][i]: # came from vertical
            j = j - 1
            txt_align.append("-")
            pat_align.append(pat[j])
            continue
        if mx[j][i-1]+1 == mx[j][i]: # came from horizontal
            i = i - 1
            txt_align.append(txt[i])
            pat_align.append("-")
            continue

    txt_align.reverse()
    pat_align.reverse()
    print(''.join( txt_align ) )
    print(''.join( pat_align ) )

    return edit_dist

def test_edit_distance1():
    txt = "TATGTCATGC"
    pat = "TACGTCAGC"
    txt = "ABCXXX"
    pat = "ZBCZ"

    n = len(txt)
    m = len(pat)

    print("txt=" + txt)
    print("pat=" + pat)

#    print( edit_distance_rec(str1, str2, n1, n2) )
    print(edit_distance_dp_bottomup(txt, pat, n, m))

if __name__=="__main__":
#    txt = "endeR"
#    pat = "enderz"
    test_edit_distance1()
    quit()

    txt = "TATGTCATGC"
    pat = "TACGTCAGC"

    txt = "TATATGCGGCGTTT"
    pat = "GGTATGCTGGCGCTA"

    cost_matrix = { 
            ("A","A"): 0, ("A","C"): 4, ("A","G"): 2, ("A","T"): 4, ("A"," "): 8,
            ("C","A"): 4, ("C","C"): 0, ("C","G"): 4, ("C","T"): 2, ("C"," "): 8,
            ("G","A"): 2, ("G","C"): 4, ("G","G"): 0, ("G","T"): 4, ("G"," "): 8,
            ("T","A"): 4, ("T","C"): 2, ("T","G"): 4, ("T","T"): 0, ("T"," "): 8,
            (" ","A"): 8, (" ","C"): 8, (" ","G"): 8, (" ","T"): 8,
    }
    score_matrix = { 
            ("A","A"): 2, ("A","C"):-4, ("A","G"):-4, ("A","T"):-4, ("A"," "):-6,
            ("C","A"):-4, ("C","C"): 2, ("C","G"):-4, ("C","T"):-4, ("C"," "):-6,
            ("G","A"):-4, ("G","C"):-4, ("G","G"): 2, ("G","T"):-4, ("G"," "):-6,
            ("T","A"):-4, ("T","C"):-4, ("T","G"):-4, ("T","T"): 2, ("T"," "):-6,
            (" ","A"):-6, (" ","C"):-6, (" ","G"):-6, (" ","T"):-6,
    }

#    txt = "TATTGGCTATACGGTT"
#    pat = "GCGTATGC"


    n = len(txt)
    m = len(pat)

    print("txt=" + txt)
    print("pat=" + pat)

#    print( edit_distance_rec(str1, str2, n1, n2) )
#    print(edit_distance_dp_bottomup(txt, pat, n, m))
#    print(edit_distance_cost_matrix_dp_bottomup(txt, pat, n, m, cost_matrix))
    print(edit_distance_score_matrix_dp_bottomup(txt, pat, n, m, score_matrix))
    quit()
    print(edit_distance_approximate_match(txt, pat, n, m))

#keywords tsp traveling-salesman-problem dynamic-programming

cost = [    [0, 20, 42, 35],
            [20, 0, 30, 34],
            [42,30,  0, 12],
            [35,34, 12,  0]]

import sys
def tsp_rec(s_rem, s_traveled, cur_city):
    if not s_rem:
        return 0
    else:
        mn = sys.maxsize
        for next_city in s_rem:
            mn = min(mn, tsp_rec(s_rem - set([next_city]), s_traveled | set([next_city]), next_city) + cost[cur_city][next_city])
        return mn

c_city = set([0,1,2,3])
tcost = tsp_rec(c_city-set([0]), set([0]), 0)
print(tcost)

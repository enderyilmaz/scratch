fin := FileStream open: 'readlines_split_and_sum.in' mode: FileStream read.

fin linesDo: [ :line | 
        words := (line substrings) collect: [:x | x].
        sm:=words inject:0 into: [:x :y | x+(y asInteger) ]. 
        Transcript show: (sm asString);cr.
    ].
fin close.

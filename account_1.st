Object subclass: Account [
    | balance |
    <comment: 'comment goes here'>
]


Account class extend [
    new [
        | r |
        <category: 'instance creation'>
        r := super new.
        r init.
        ^r
    ]
]


Account extend [
    init [
        <category: 'initialization'>
        balance := 0
    ]
]


a := Account new.


Account inspect.


a yourself.


import sys

def solve_rec(n, K):
    if K==1:
        return 1
    else:
        cum = 0
        for i in range(n):
            cum = cum + solve_rec(n-i, K-1)
        return cum
def solve_rec_dbg(n, K, str1):
    if K==1:
        print(str1 + ", " + str(n))
        return 1
    else:
        cum = 0
        for i in range(n):
            cum = cum + solve_rec_dbg(n-i, K-1, str1 + ", " + str(i))
        return cum

def solve_dp_top_down(n, K, db={}):
    if K==1:
        return 1
    else:
        cum = 0
        for i in range(n):
            sm = 0
            if (n-i, K-1) in db:
                sm = db[ (n-i, K-1) ]
            else:
                sm = solve_dp_top_down(n-i, K-1, db)
                db[ (n-i, K-1) ] = sm
            cum = cum + sm
        return cum
if __name__=="__main__":
    n = 1000
    K = 100
#    print( solve_rec(n, K) )
    print( solve_dp_top_down(n, K) )


fin := FileStream open: 'sample.txt' mode: FileStream read.
lines := OrderedCollection new.
fin linesDo: [ :line | lines add: line ].
fin close.

lines do: [:line | Transcript nextPutAll: line].

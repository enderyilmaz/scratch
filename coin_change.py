#keywords dynamic-programming

v = 10
coins = [2,5]

import sys
import functools

def change_rec(rem):
    if rem == 0:
        return 0
    v = []
    ncoins = len(coins)
    for coin_i in range(ncoins):
        x = rem - coins[coin_i]
        v.append( change_rec(x) + 1 if x>=0 else sys.maxsize )
    return functools.reduce(min, v)

def change_rec2(rem, coin_i):
    if rem == 0:
        return 0
    ncoins = len(coins)
    v1 = sys.maxsize 
    v0 = change_rec2(rem, coin_i-1) if coin_i>0 else sys.maxsize
    if rem>=coins[coin_i]:
        v1 = change_rec2(rem-coins[coin_i], coin_i)+1
    return min(v0, v1)

def change_dp(v):
    dp = [0 for _ in range(v+1)]
    change = v
    for i in range(1,v+1):
        v_ = 0
        dp[i] = sys.maxsize
        for j in range(len(coins)):
            v_ = dp[i-coins[j]]+1 if coins[j]<=i else sys.maxsize
            dp[i] = v_ if v_<dp[i] else dp[i]
    return dp[-1]

def ways_rec(change, coin_i_max):
    if coin_i_max==-1:
        return 0
    if change == 0:
        return 1
    rem = change - coins[coin_i_max]
    v0 = 0
    if rem>=0:
        v0 = ways_rec(rem, coin_i_max)
    v1 = ways_rec(change, coin_i_max-1)
    return v0 + v1

#def ways_dp(change):
#    """ WRONG !!!
#    """
#    n=len(coins)
#    dp = [[0]*(change+1) for _ in range(n)]
#    for c in range(1,change+1):
#        dp[0][c] = 1 if c%coins[0]==0 else 0
#    for i in range(1,n):
#        for c in range(change+1):
#            v0 = dp[i-1][c]
#            v1 = 1+dp[i-1][c-coins[i]] if (c%coins[i]==0 and coins[i]<=c) else 0
#            dp[i][c] = v0 + v1
#    for i in range(n):
#        for c in range(change+1):
#            print("%3d" % dp[i][c], end='')
#        print("")
#    return dp[-1][-1]

def ways_dp_bup(change):
    ncoins = len(coins)
    dp = [[0]*(change+1) for _ in range(ncoins)]
    for i in range(ncoins):
        dp[i][0] = 1
    for c in range(change+1):
        dp[0][c] = 1
    for i in range(ncoins):
        for c in range(change+1):
            v0 = dp[i-1][c]
            v1 = dp[i][c-coins[i]] if c>=coins[i] else 0
            dp[i][c] = v0 + v1
    return dp[-1][-1]



print(change_dp(10))
print("rec: " + str(change_rec(v)) )
print("rec2: " + str(change_rec2(v,len(coins)-1)))
print(ways_rec(10, len(coins)-1))
#print(ways_dp(10))

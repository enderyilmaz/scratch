
fin := FileStream open: 'sample.txt' mode: FileStream read.
fin linesDo: [ :line | Transcript nextPutAll: line; nl ].
fin position.
25 timesRepeat: [ Transcript nextPut: (fin next) ].
fin close.

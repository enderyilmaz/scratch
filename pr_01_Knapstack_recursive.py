import sys
import functools
import itertools


def knap_rec(idx, wavail):
    if idx==len(weight) or wavail==0:
        return 0
    if wavail>=weight[idx]:
        v0 = knap_rec(idx+1, wavail)
        v1 = knap_rec(idx+1, wavail - weight[idx])
        return max(v1 + val[idx], v0)
    else:
        return knap_rec(idx+1, wavail)

def knap2_rec(idx, wavail):
    if idx==len(weight) or wavail==0:
        return 0
    v0 = knap2_rec(idx+1, wavail)
    v1 = 0
    if wavail>=weight[idx]:
        v1 = knap2_rec(idx+1, wavail - weight[idx]) + val[idx]
    return max(v1, v0)

def knap_dp(w,v,wmax):
    n=len(w)
    dp=[[0]*(wmax+1) for _ in range(n)]
    for c in range(wmax+1): # c = capacity
        dp[0][c] = v[0] if w[0]<=c else 0

    for i in range(1,n):
        for c in range(wmax+1):
            v0 = dp[i-1][c]
            v1 = 0
            if w[i]<=c:
                v1 = dp[i-1][c-w[i]] + v[i]
            dp[i][c] = max(v0, v1)

#    for i in range(n):
#        for j in range(wmax+1):
#            print("%3d" % (dp[i][j]), end='')
#        print("")

    # trace
    i = n-1
    c=wmax
    select_list = []
    while True:
        if i==0:
            if dp[i][c]>0:
                select_list.append(i)
            break
        elif dp[i][c]==dp[i-1][c] and i>0:
            i = i - 1
        else:
            select_list.append(i)
            c = c - w[i]
            i = i - 1
    select_list.reverse()
    for item_i in select_list:
        print("selected [%3d] W=%3d V=%3d" % (item_i, w[item_i], v[item_i]))

def knap_gen(wavail):
    mx_val = 0
    for select_v in itertools.product([0,1], repeat=len(weight)):
        sm_weight = functools.reduce( lambda x,y: x+y ,[w for s,w in zip(select_v, weight) if s==1], 0)
        sm_val    = functools.reduce( lambda x,y: x+y ,[w for s,w in zip(select_v, val   ) if s==1], 0)
        if sm_weight<=wavail and mx_val<sm_val:
            mx_val = sm_val
    return mx_val

def knap_dyn_top_down():
    ""
def knap_dyn_down_up():
    ""

import random

if __name__=="__main__":
    weight = [1 ,2 , 3]
    val    = [10,15,40]
    wmax = 4
    weight = [3,4,5,9,4]
    val    = [3,4,4,10,4]
    wmax = 11
    
    n = 1000
    weight = [random.randint(1,20) for _ in range(n)]
    val    = [random.randint(1,50) for _ in range(n)]
    wmax = 50

    knap_dp(weight, val, wmax)
    quit()

    mx = knap2_rec(0, wmax)
    print(mx)
    print(knap_gen(wmax))

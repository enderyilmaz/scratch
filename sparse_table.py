#keywords sparse table
import math
import functools

def buildSparseTable_min(a):
    n = len(a)
    imax = int(math.log2( n )) + 1
    dp = [ [0]*n for _ in range(n) ]
    for j in range(n):
        dp[0][j] = a[j]
    for i in range(1,imax):
        shift = 1<<(i-1)
        for j in range(0, n-shift):
            dp[i][j] = min(dp[i-1][j], dp[i-1][j+shift])
#    for i in range(imax):
#        shift = 1<<(i)
#        for j in range(n-shift+1):
#            print("%3d " % (dp[i][j]), end='')
#        print("")
    return dp
def query(sparse_table, start_i, end_i):
    i = int(math.log2( end_i-start_i ))
    j0 = start_i
    j1 = end_i - (1<<i)
    return min(sparse_table[i][j0], sparse_table[i][j1])
    print(j0, j1)
    
if __name__=="__main__":
    a = [7, 2, 3, 0, 5, 10, 3, 12, 18]
    n = len(a)
    
    st_min = buildSparseTable_min(a)
    for i in range(n-1):
        for j in range(i+1,n):
            assert( query(st_min,i,j) == functools.reduce(min, a[i:j]) )
    print(query(st_min, 1,6))

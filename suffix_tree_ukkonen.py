# suffix tree attempt #4




anode = 0
aedge_i= 0
alen  = 0
pos   = 0
txt   = ""
rem   = 0
node_list = []

def new_node(start, end):
    new_node = [start, end, 0, {}]
    node_list.append( new_node )
    return len(node_list)-1

def init():
    global anode, aedge_i, alen, pos, txt
    global node_list, rem
    rem = 0
    anode = 0
    aedge_i= 0
    alen  = 0
    pos   = -1
    txt   = ""
    node_list = []
    new_node(-1, -1)

def edge_len(node_idx):
    start_, end_, slink_, map_ = node_list[ node_idx ]
    end__ = end_ if end_>0 else pos+2
    return end__ -  start_

def aedge():
    return txt[aedge_i]

def active_edge_node_idx():
    return node_list[ anode ][3][aedge()]

def walk():
    global alen, anode, aedge_i
    if alen == 0:
        return False
    edgelen = edge_len( active_edge_node_idx() )
    walked = False
    while edgelen<=alen:
        start_, end_, slink_, map_ = node_list[active_edge_node_idx()]
        anode = active_edge_node_idx()
        aedge_i = aedge_i + edgelen
        alen = alen - edgelen
        walked = True
        if alen == 0:
            break
        edgelen = edge_len( active_edge_node_idx() )
    return walked
    
def getNextC():
    walk()
    if alen == 0: # @ edge. 
        if aedge() in node_list[ anode ][3]:
            return aedge()  # already in. -> match
        else:
            return 0        # @ edge and not in tree -> mismatch
    else:
        start_, end_, slink_, map_ = node_list[active_edge_node_idx()]
        return txt[ start_ + alen ]


def add(c):
    global anode, aedge_i, alen
    global pos, rem, txt
    pos = pos + 1
    rem = rem + 1
    next_slink = 0
#    txt = txt + c

    while rem>0:
        aedge_i = pos if alen==0 else aedge_i
        nextC = getNextC()
#        print("c=%s, nextC=%s" % (c, nextC))
        if nextC == c: # already in
            alen = alen + 1
            if next_slink > 0:
                node_list[ next_slink ][2] = anode
            next_slink = anode
            return
        else: # mismatch
            if alen==0: # @ edge
                newNode = new_node(pos, -1)
                node_list[ anode ][3][c] = newNode
#                rem = rem - 1
#                anode = 0
#                continue
                if next_slink > 0:
                    node_list[ next_slink ][2] = anode
                next_slink = anode
            else: # split
                start_, end_, slink_, map_  = node_list[active_edge_node_idx()]
                node                        = node_list[active_edge_node_idx()]
                # create new nodes
                mnode = new_node(start_, start_ + alen)
                nnode = new_node(pos, -1)
                # re-set starting point
                node[0] = start_ + alen
                # set pointers
                node_list[mnode][3][ txt[start_ + alen] ] = node_list[anode][3][ aedge() ] 
                node_list[mnode][3][ c ] = nnode
                node_list[anode][3][ aedge() ] = mnode
                # set slink
                if next_slink > 0:
                    node_list[ next_slink ][2] = mnode
                next_slink = mnode

        slink = node_list[ anode ][2]
        if slink>0:
            anode = slink
            rem = rem - 1
        else:
            rem = rem - 1
            if alen>0 and anode==0:
                alen = alen - 1 
                aedge_i = pos - rem + 1 
            anode = 0


def dump():
    print("rem  =%3d" %  rem)
    print("alen =%3d" % alen)
    print("anode=%3d" % anode)
    print("aedge=%3s" % str(aedge()))
    print("pos  =%3d" % pos )
    for i, node in enumerate(node_list):
        start_ = node[0]
        end_   = node[1] if node[1]>=0 else pos + 2
        print("[%2d] %3d, %3d, %3d, %s, %s" % (i, node[0], node[1], node[2], str(node[3]), txt[start_:end_]))

def getSuffix(concatSep=""):
    node = 0
    stack = []
    for node_idx in node_list[0][3].values():
        stack.append( ("", node_idx) )
    while( stack ):
        str1, node_idx_ = stack.pop()
        new_node_list   = node_list[ node_idx_ ][3].values()
        if not new_node_list: # leaf
            start_ = node_list[ node_idx_ ][0]
            end_   = node_list[ node_idx_ ][1]
            end_   = end_ if end_>0 else pos+2
            yield str1 + txt[start_:end_], node_idx_
        else:
            for new_node_ in new_node_list:
                start_ = node_list[ node_idx_ ][0]
                end_   = node_list[ node_idx_ ][1]
                end_   = end_ if end_>0 else pos+2
                lastChar = txt[end_-1:end_]
                if node_list[ node_idx_ ][2]>0:
                    lastChar = lastChar.upper()
                stack.append([str1 + txt[start_:end_-1]+ lastChar + concatSep, new_node_])
def dumpSuffix():
    suffix_list = [(suffix, node_idx) for suffix,node_idx in getSuffix(">")]
    print(txt)
    suffix_list.sort()
    for suffix, node_idx in suffix_list:
        print("%3d %s" % (node_idx, suffix))
    suffix_list = [(suffix, node_idx) for suffix,node_idx in getSuffix()]
    suffix_list.sort(key=lambda x: len(x[0]))
    for suffix, node_idx in suffix_list:
        print(suffix)

def powerset(seq):
    """
    Returns all the subsets of this set. This is a generator.
    """
    if len(seq) <= 1:
        yield seq
        yield []
    else:
        for item in powerset(seq[1:]):
            yield [seq[0]]+item
            yield item

def test():
    suffix_list = [(suffix, node_idx) for suffix,node_idx in getSuffix()]
    suffix_list.sort(key=lambda x: len(x[0]))
    testPass = True
    sz = 0
    for suffix, node_idx in suffix_list:
        sz = sz + 1
        if len(suffix)!=sz:
            print("suffix failed: %s->%s" % (txt, suffix))
            testPass = False
            break
    for idx,node in enumerate(node_list):
        end_ = node[1] if node[1]>-1 else pos+1
        if (node[0],node[1])==(-1,-1):
            continue
        if end_-node[0]<1:
            print("suffix failed: %s@%d, end_==%d, node[0]=%d" % (txt, idx, end_, node[0]))
            dump()
            testPass = False
            break
    return testPass

def find_all(pat):
    node_idx = 0
    node = node_list[node_idx] # get the root node
    acc_len = 0
    pat_ = pat

    if pat[0] not in node[3]:
        return

    node_idx = node[3][pat[0]]

    while len(pat_)>0:
        szpat = len(pat_)
        c = pat_[0]
        start_, end_, slink_, map_ = node_list[ node_idx ]
        end_ = end_ if end_>0 else len(txt)
        if szpat>(end_-start_): # pattern longer than edge length
            if pat_[0: end_-start_] != txt[start_:end_]:
                return
#                return -1
            if pat_[end_ - start_] in map_:
                pat_ = pat_[end_-start_:]
                acc_len = acc_len + end_ - start_
                node_idx = map_[ pat_[0] ]
                continue
            else:
                return
#                return -1
        else: # pattern shorter than edge length
            if txt[start_:].startswith(pat_): # MATCH found
                pat_ = ""
                # visit all child nodes
                # second path of the algorithm
                visit_nodes = set([])
                visit_nodes.add( (node_idx, acc_len) )
                while visit_nodes:
                    node_idx_, sub_len = visit_nodes.pop()
                    start_, end_, slink_, map_ = node_list[ node_idx_ ]
                    end_ = end_ if end_>0 else len(txt)
                    if not map_.values(): # leaf node
#                        print(start_ - sub_len)
                        yield start_ - sub_len
                    else:
                        for v in map_.values(): # add childs to queue
                            visit_nodes.add( (v, end_-start_+sub_len) )
            else:
                pat_ = ""
                return


def getRepeating(nmin=1):
    queue = [ (0, "") ]
    while(queue):
        node_idx, acc_txt = queue.pop()
        start_, end_, _, map_ = node_list[node_idx]
        if map_.values(): # not leaf
            str1 = txt[start_:end_]
            if len(acc_txt)+len(str1)>=nmin:
                print(acc_txt + str1)
            for v in map_.values():
                queue.append((v, acc_txt + str1))
def getLongestRepeating():
    nmin = 1
    queue = [ (0, "") ]
    lst_longest = []
    while(queue):
        node_idx, acc_txt = queue.pop()
        start_, end_, _, map_ = node_list[node_idx]
        if map_.values(): # not leaf
            str1 = txt[start_:end_]
            if len(acc_txt)+len(str1)>=nmin:
                nmin = len(acc_txt) + len(str1) + 1
                lst_longest.append( acc_txt + str1 )
            for v in map_.values():
                queue.append((v, acc_txt + str1))
    lst_longest.sort(key=len)
    return lst_longest[-1]

from itertools import product
from random import randrange
import sys
import guppy
import gc


if __name__=="__main__":
    case = 4
    if case==0:
#    str1 = "abcdabcxabcd$"
#    str1 = "acbabcbabcbbaaaaacbabcbacbbacba$"
#   str1 = "abcbabcbbaaaaacbabcbacbbacba$"
#    str1 = "abcbabcbbacbab$"
#    str1 = "abcbabcbbacb$"
#    str1 = "aaaabababcaac$"
#    str1 = "aabaabcaaba$"
#    str1 = "aabaabcaa$"

#    str1 = "aaababcaaba$"
#2    str1 = "aaababcaaba"

#    str1 = "abcabxabcd$"
#    str1 = "abcbab$"
#    str1 = "acbabcbabc$"
#    str1 = "banana$"
        str1 = "aab$"
        str1 = "aabab$"
        str1 = "aabaaa$"
        str1 = "xabctttabcqabcgtgttt$"

        init()
        txt = str1
        for c in str1:
            add(c)
        dump()
        dumpSuffix()

        getLongestRepeating()

    elif case==1:
        lst = ["a","b","c"]
        for perm in product(lst, repeat=8):
            init()
            str1 = "a" + ''.join(perm)+"$"
            txt = str1
            for c in str1:
                add(c)
            if test():
                print("pass: " + str1)
            else:
                dump()
                dumpSuffix()
                break
    elif case==2:
        lst = ["a","b","c"]
        sz = 7000000
        init()
        str1_list = []
        for i in range(sz):
            str1_list.append( lst[ randrange(0, len(lst) ) ] )
        txt = ''.join(str1_list) + "$"


        cntr = 0
        gc.enable()
        for c in txt:
            if cntr % 1000000 == 0:
                gc.collect()
            add(c)
            cntr = cntr + 1

        print("sz node_list=" + str(len(node_list)))
        quit()
        if test():
            pass
#            print("pass: " + str1)
        else:
            dump()
            dumpSuffix()
    if case==3:
        str1 = "xabctttabcqabcgtgt$"
        str1 = "aab$"

        for _ in range(10):
            lst = ["a","b","c"]
            sz = 1000
            init()
            str1_list = []
            for i in range(sz):
                str1_list.append( lst[ randrange(0, len(lst) ) ] )
            txt = ''.join(str1_list) + "$"
            for c in txt:
                add(c)

#            dump()
#            dumpSuffix()

            for _ in range(100):
                pat_start = randrange(0, len(txt)-1)
                pat_end   = randrange(pat_start+1, len(txt)+1)
                pat = txt[pat_start:pat_end]
                print(txt)
                print("pat start=%d, end=%d" % (pat_start, pat_end))
                for fnd_idx in find_all(txt[pat_start:pat_end]):
                    fnd_pat = txt[fnd_idx: fnd_idx+pat_end-pat_start]
                    print("correct=%d, found=%d, len=%d (%s) %s" % (pat_start, fnd_idx, pat_end-pat_start, pat, fnd_pat))
    if case==4: # longest repeating
        str1 = "xabctttabcqabcgtgt$"

        for _ in range(1):
            lst = ["a","b"]
            sz = 1000000
            init()
            str1_list = []
            for i in range(sz):
                str1_list.append( lst[ randrange(0, len(lst) ) ] )
            txt = ''.join(str1_list) + "$"
            for c in txt:
                add(c)

#            dump()
#            dumpSuffix()

            str1 = getLongestRepeating()
            print(str1)





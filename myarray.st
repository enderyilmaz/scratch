
Object subclass: MyArray [
    | data_ |
    MyArray class >> new: sz [
        | r |
        r := super new.
        r init: sz.
        ^r
    ]    
    init: sz [
        data_ := Array new: sz.
    ]
    init_inc [
        data_ := 1 to: (data_ size) collect: [ :x | x ]
    ]
    reverse [
        | temp |
        temp := Array new: (data_ size).
        data_ := 1 to: (data_ size) collect: [ :x | (data_ size)-x+1]
    ]
]

ma := MyArray new: 10.
ma init_inc.
ma inspect.
ma reverse.
ma inspect.

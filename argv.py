import sys

fileName = sys.argv[1]
print(fileName)
fin = open(fileName, "r")

line_no = 0
for line in fin.readlines():
    line_no = line_no + 1
    print("{}:{}".format(line_no, line.strip()))


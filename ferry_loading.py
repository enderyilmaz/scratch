#keywords dynamic-programming knapsack
import sys

"""
notes: possible variations of this problem
in order:= not vehicle skipping
out order:=can skip some vehicles
1) in/out order max # of vehicles
2) in/out order max val of vehicles
3) in/out order max # of vehicles, constraint->  |delta|<2
4) in/out order max val of vehicles, constraint->|delta|<2
"""

def rec0(rem0, rem1, car_i, car_list):
#    print(rem0, rem1, car_i, car_list)
    if car_i==len(car_list):
        if abs(rem0-rem1)>2:
            return -sys.maxsize
        else:
            return 0
    v0 = 1+rec0(rem0-car_list[car_i], rem1, car_i+1, car_list) if rem0>=car_list[car_i] else (0)
    v1 = 1+rec0(rem0, rem1-car_list[car_i], car_i+1, car_list) if rem1>=car_list[car_i] else (0)
    if v0==0 and v1==0:
        return (0)
    return max(v0, v1)
def rec(rem0, rem1, car_i, car_list):
#    print(rem0, rem1, car_i, car_list)
    if car_i==len(car_list):
        return (0 , [])
    v0, path0 = rec(rem0-car_list[car_i], rem1, car_i+1, car_list) if rem0>=car_list[car_i] else (0, [])
    v1, path1 = rec(rem0, rem1-car_list[car_i], car_i+1, car_list) if rem1>=car_list[car_i] else (0, [])
    v0 = v0 + 1 if rem0>=car_list[car_i] else v0
    v1 = v1 + 1 if rem1>=car_list[car_i] else v1
    if v0==0 and v1==0:
        return (0, [])
    elif v0>=v1:
        path0.append( "port" )
        return v0, path0[:] 
    else:
        path1.append( "starboard" )
        return v1, path1[:]

def dp_bup2(ferry_len, car_list):
    n = len(car_list)
    dp = [list([[0]*(ferry_len+1) for _ in range(ferry_len+1)]) for _ in range(n+1)]
    print(len(dp), len(dp[0]), len(dp[0][0]))
#    for i in range(1,n+1):
#        dp[i][0][0] = 1
    for i in range(1,n+1):
        for c0 in range(ferry_len+1):
            for c1 in range(ferry_len+1):
                v0, v1 = 0, 0
                if c0>=car_list[i-1]:
                    v0 = 1 + dp[i-1][c0-car_list[i-1]][c1]
                if c1>=car_list[i-1]:
                    v1 = 1 + dp[i-1][c0][c1-car_list[i-1]]
                dp[i][c0][c1] = max(v0, v1)

#    for i in range(1,n+1):
#        for c0 in range(ferry_len+1):
#            for c1 in range(ferry_len+1):
#                print("%3d" % (dp[i][c0][c1]), end='')
#            print("")
#        print("\n")
    print( dp[-1][-1][-1] )

def dp_bup(ferry_len, car_list):
    """ not working !!! 
    """
    n = len(car_list)
    dp0 = [[0]*(ferry_len+1) for _ in range(n+1)]
    dp1 = [[0]*(ferry_len+1) for _ in range(n+1)]
    for i in range(1,n+1):
        for c in range(1,ferry_len+1):
            v0, v1 = 0, 0
            if c>=car_list[i-1]:
                v11 = 1 + dp1[i-1][c-car_list[i-1]]
                v10 = 0 + dp1[i-1][c]
            if c>=car_list[i-1]:
                v01 = 1 + dp0[i-1][c-car_list[i-1]]
                v00 = 1 + dp0[i-1][c]
            if v11+v00>v10+v01:
                dp1[i][c] = v11
            else:
                dp0[i][c] = v01
    for i in range(1,n+1):
        for c in range(1,ferry_len+1):
            print("%3d" % (dp0[i][c]), end='')
        print("")
    print("")
    for i in range(1,n+1):
        for c in range(1,ferry_len+1):
            print("%3d" % (dp1[i][c]), end='')
        print("")


def process(ferry_len, car_list):
    print(ferry_len)
    print(car_list)
    dp_bup2(ferry_len, car_list)
    return


    sz, lst = rec(ferry_len, ferry_len, 0, car_list)
    print(sz)
    for item in lst:
        print(item)

if __name__=="__main__":
    ncase = int(sys.stdin.readline().strip())
    for case_i in range(ncase):
        sys.stdin.readline()
#        ferry_len = 100*int(sys.stdin.readline().strip())
        ferry_len = 1*int(sys.stdin.readline().strip())
        car_list = []
        while True:
            line = sys.stdin.readline()
            car_len = int(line.strip())
            if car_len==0:
                break
            car_list.append( car_len )
        process(ferry_len, car_list)
    car_list = [10, 20, 30, 40, 50, 60]

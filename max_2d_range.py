#keywords 2d-range-sum
import sys
A=[ [0,-2,-7,0],
    [9,2,-6,2],
    [-4,1,-4,1],
    [-1,8,0,-2]]
B=[ [ 0,-2,-9,-9],
    [ 9, 9,-4, 2],
    [ 5, 6,-11,-8],
    [ 4,13,-4,-3]]
C=[ [ 0,-2,-9,-9],
    [ 9, 9,-4, 2],
    [ 5, 6,-11,-6],
    [4,13,-4,-3]]

M = A

def cumMatrix(M):
    m = len(M)
    n = len(M[0])
    for i in range(m):
        for j in range(n):
            if i>0:
                M[i][j] = M[i][j] + M[i-1][j] 
            if j>0:
                M[i][j] = M[i][j] + M[i][j-1] 
            if i>0 and j>0:
                M[i][j] = M[i][j] - M[i-1][j-1] 
    return M

def sumMatrixRange(cM,i,j,k,l):
    sm = cM[k][l]
    if i>0:
        sm = sm - cM[i-1][l]
    if j>0:
        sm = sm - cM[k][j-1]
    if i>0 and j>0:
        sm = sm + cM[i-1][j-1]
    return sm

def dumpMatrix(M):
    m = len(M)
    n = len(M[0])
    for i in range(m):
        for j in range(n):
            print("%3d" % M[i][j], end='')
        print("")

def findMaxRange_On4(M):
    m = len(M)
    n = len(M[0])
    cM = cumMatrix(M)
    mx_idx= None
    mx = -sys.maxsize
    for i in range(m):
        for j in range(n):
            for k in range(i,m):
                for l in range(j,n):
                    sm = sumMatrixRange(cM, i, j, k, l)
                    if sm>mx:
                        mx = sm
                        mx_idx = (i,j,k,l)
    print(mx_idx)
    print(mx)

dumpMatrix(A)
print("")
cM = cumMatrix(A)
dumpMatrix(cM)
print("")

print( sumMatrixRange(cM,1,2,3,3))
findMaxRange_On4(A)


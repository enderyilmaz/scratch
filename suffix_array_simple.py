#Suffix arrays and BWT
#keywords suffix-array suffix-array-applications

import functools

def suffixArray(txt):
#    return sorted(range(len(txt)), cmp=lambda i,j: 1 if txt[i:]>=txt[j:] else -1)
    return sorted(range(len(txt)), key=functools.cmp_to_key( lambda i,j: 1 if txt[i:]>=txt[j:] else -1))

def findFirst(pat, txt, suffixarray):
    lo, hi = 0, len(txt)
    while(lo<hi):
        middle = int((lo+hi)/2)
        if txt[suffixarray[middle]:] < pat:
            lo = middle + 1
        else:
            hi = middle
    return lo

def findLast(pat, txt, suffixarray):
    lo, hi = 0, len(txt)
    while lo<hi:
        middle = int((lo+hi)/2)
        if txt[suffixarray[middle]:suffixarray[middle]+len(pat)] <= pat:
            lo = middle + 1
        else:
            hi = middle
    return lo

def computeLCP(txt, suffixarray):
    m = len(txt)
    lcp = [0 for i in range(m)]
    for i in range(1,m):
        u = suffixarray[i-1]
        v = suffixarray[i]
        n = 0
        while txt[u] == txt[v]:
            n += 1
            u += 1
            v += 1
            if (u>=m) or (v>=m):
                break
        lcp[i] = n
    return lcp

def test_computeLCP():
    t = "amanaplanacanalpanama"
    sa = suffixArray(t)
    lcp = computeLCP(t, sa)
    print("SA,LCP,Suffix")
    for i, j in enumerate(sa):
        print("%2d: %2d %s"% (j, lcp[i], t[j:]))

def test_suffixArray():
    t = "amanaplanacanalpanama"
    sa = suffixArray(t)
    print(sa)
    for i in sa:
        print("%2d: %s"% (i, t[i:]))

def test_findFirst():
    t = "amanaplanacanalpanama"
    sa = suffixArray(t)
    first = findFirst("an", t, sa)
    print(t)
    print(first, sa[first], t[sa[first]:])

def test_findLast():
    t = "amanaplanacanalpanama"
    sa = suffixArray(t)
    print(t)
    first = findFirst("an", t, sa)
    last = findLast("an", t, sa)
    for suffix in sa[first:last]:
        print(suffix, t[suffix:])
    print(last-first)


if __name__=="__main__":
#    test_suffixArray()
#    test_findFirst()
#    test_findLast()
    test_computeLCP()


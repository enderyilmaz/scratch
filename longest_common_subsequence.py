# longest common subsequence
#keywords longest-common-subsequence lcs

def lcs_rec(txt, pat, n, m):
    if n==0 or m==0:
        return 0
    if txt[n-1]==pat[m-1]:
        return 1 + lcs_rec(txt, pat, n-1, m-1)
    else:
        return max( lcs_rec(txt, pat, n-1, m), lcs_rec(txt, pat, n, m-1) )

def lcs_dp(txt, pat, n, m):
    """
    bottom_up: table method
    number only
    """
    mx = [ [0]*(n+1) for _ in range(m+1) ]
    for i in range(n+1):
        for j in range(m+1):
            if i==0 or j==0:
                mx[j][i] = 0
            else:
                delta = 1 if txt[i-1]==pat[j-1] else 0
                v1 = mx[j-1][i-1] + delta
                v2 = mx[j-1][i]
                v3 = mx[j][i-1]
                mx[j][i] = max( max(v2,v3), v1 )
    return mx[m][n]

def lcs_list_dp(txt, pat, n, m):
    """
    bottom_up: list method
    number, and list
    """
    mx = [ [0]*(n+1) for _ in range(m+1) ]
    for i in range(n+1):
        for j in range(m+1):
            if i==0 or j==0:
                mx[j][i] = 0
            else:
                delta = 1 if txt[i-1]==pat[j-1] else 0
                v1 = mx[j-1][i-1] + delta
                v2 = mx[j-1][i]
                v3 = mx[j][i-1]
                mx[j][i] = max( max(v2,v3), v1 )
    print(txt)
    print(pat)
    for j in range(m+1):
        for i in range(n+1):
            print("%3d" % (mx[j][i]),end='')
        print("")
    return mx[m][n]

def test1():
    txt = "ender"
    pat = "yienmaz"

    n = len(txt)
    m = len(pat)

    print(lcs_rec(txt, pat, n, m))
    print(lcs_dp(txt, pat, n, m))
if __name__=="__main__":
    lst_txt = [50,3,10,4]
    lst_pat = [5,3,4,6]
    n = len(lst_txt)
    m = len(lst_pat)

#    lcs = lcs_list_dp(lst_txt, lst_pat, n, m)
    lcs = lcs_dp(lst_txt, lst_pat, n, m)
    print(lcs)

#keywrods: dynamic-programming maximize-value-of-expression
import sys
def maximizeExpression(a):
    """
    assume expression=A[s] - A[r] + A[q] - A[p], s>r>q>p
    """
    n = len(a)
    minint = -sys.maxsize
    first = [minint]*(n+1)
    second= [minint]*n
    third = [minint]*(n-1)
    fourth= [minint]*(n-2)
    
    for i in range(n-1,-1,-1):
        first[i] = max( first[i+1], a[i])
    for i in range(n-2,-1,-1):
        second[i] = max( second[i+1], first[i+1]-a[i])
    for i in range(n-3,-1,-1):
        third[i] = max( third[i+1], second[i+1]+a[i])
    for i in range(n-4,-1,-1):
        fourth[i] = max( fourth[i+1], third[i+1]-a[i])
    return fourth[0]

if __name__=="__main__":
    a = [3, 9, 10, 1, 30, 40]

    print( maximizeExpression(a) )

#keywords: coin-change
import sys

#coin_list=[1,2,5,10,20,50]
coin_list=[2,5]

def cc_rec(rem,coin_list, coin_i):
    if rem==0:
        return 0
    x = rem-coin_list[coin_i]
    v0, v1 = sys.maxsize, sys.maxsize
    if x>=0:
        v1 = cc_rec(x, coin_list, coin_i)+1
    if coin_i>0:
        v0 = cc_rec(rem, coin_list, coin_i-1)
    return min(v0, v1)

def coin_change_rec(n):
    def cc_rec(rem,coin_list, coin_i):
        if rem==0:
            return 0
        x = rem-coin_list[coin_i]
        v0, v1 = sys.maxsize, sys.maxsize
        if x>=0:
            v1 = cc_rec(x, coin_list, coin_i)+1
        if coin_i>0:
            v0 = cc_rec(rem, coin_list, coin_i-1)
        return min(v0, v1)
    return cc_rec(n, coin_list, len(coin_list)-1)

def cc_dp_bup(rem):
    ncoin = len(coin_list)
    dp = [sys.maxsize]*(rem+1)
    dp[0] = 0
    for c in range(rem+1):
        for coin_i in range(ncoin):
            x = c - coin_list[coin_i]
            v1 = dp[x] + 1 if x>=0 else sys.maxsize
            dp[c] = v1 if v1<dp[c] else dp[c]

    return (dp[-1])

def ways_rec(rem, coin_list, coin_i):
    if coin_i==-1:
        return 0
    if rem==0:
        return 1
    x = rem - coin_list[ coin_i ]
    v1 = 0
    if x>=0:
        v1 = ways_rec(x, coin_list, coin_i) 
    v0 = ways_rec(rem, coin_list, coin_i-1)
    return v0 + v1

def ways_dp_bup(rem, coin_list):
    ncoin = len(coin_list)
    dp = [[0]*(rem+1) for _ in range(ncoin)]
    for coin_i in range(ncoin):
        dp[coin_i][0] = 1
    for c in range(rem+1):
        dp[0][c] = 1
    for coin_i in range(ncoin):
        for c in range(rem+1):
            v0 = dp[coin_i-1][c]
            x = c - coin_list[coin_i]
            v1 = 0
            if x>=0:
                v1 = dp[coin_i][x]
            dp[coin_i][c] = v0 + v1
    return dp[-1][-1]


if __name__=="__main__":
    rem = 15
#    num_cc = coin_change_rec(rem)
#    print(num_cc)
#    num_cc = cc_dp_bup(rem)
#    print(num_cc)
    for rem in range(2,20):
        num_ways0 = ways_rec(rem, coin_list, len(coin_list)-1)
#        print(num_ways)
        num_ways1 = ways_dp_bup(rem, coin_list)
#        print(num_ways)
        print("%3d: %3d %3d" % (rem, num_ways0, num_ways1))


fact1 := [ :x |
    (x==0)
        ifTrue: [1]
    x * fact1 value: (x-1)
]

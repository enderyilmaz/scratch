"Loops"
 1 to: 20 do: [:x | x printNl ]
 1 to: 20 by: 2 do: [:x | x printNl ]
 20 to: 1 by: -1 do: [:x | x printNl ]
 
 
i := Interval from: 5 to: 10
i do: [:x | x printNl]

i := (Interval from: 5 to: 10 by: 2)
i do: [:x| x printNl]

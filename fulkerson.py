#keywords: max-flow fulkerson
import sys
import random
import math

def graph_dump(gAdj):
    n = len(gAdj)
    for row in gAdj:
        for c in row:
            print("%3d" % (c), end='')
        print("")

def graph_p2p_bfs(gAdj, n0 ,n1, parent):
    stack = []
    stack.append( n0 )
    if n0==n1:
        return True
    n = len(gAdj)
    visited = [False]*n
    visited[n0] = True
    while(stack):
        node = stack.pop(0)
        for node2 in range(n):
            if gAdj[node][node2]>0 and (not visited[node2]):
                parent[node2] = node
                stack.append(node2)
                visited[node2] = True
    return visited[n1]

def maxflow_ford_fulkerson(gAdj, from_node, to_node):
    n = len(gAdj)
    madj = [ row[:] for row in gAdj]
    parent = [-1]*n
    total_flow = 0
    while graph_p2p_bfs(madj, from_node, to_node, parent):
        mx_flow = sys.maxsize
        node = to_node
        while node != from_node:
            prev_node = parent[node]
            mx_flow = min(mx_flow, madj[prev_node][node])
            node = prev_node
        total_flow = total_flow + mx_flow
        node = to_node
        while node != from_node:
            prev_node = parent[node]
            madj[ prev_node ][node] = madj[ prev_node ][node] - mx_flow
            madj[ node ][prev_node] = madj[ node ][prev_node] + mx_flow
            node = prev_node
    return total_flow


def graph_make_unidirectional(gAdj):
    n = len(gAdj)
    m = len(gAdj[0])
    for i in range(n):
        for j in range(m):
            if gAdj[i][j]>0:
                gAdj[j][i] = gAdj[i][j]

def graph_isolated_nodes(gAdj):
    n = len(gAdj)
    not_visited = set( list( range(n) ) )
    iso_list_list = []
    queue = []
    while not_visited:
        node_ = not_visited.pop()
        queue.append( node_ )
        visited = set([])
        visited.add( node_ )
        while queue:
            node = queue.pop(0)
            for next_node, edge in enumerate(gAdj[node]):
                if gAdj[node][next_node]>0 and (next_node not in visited):
                    queue.append( next_node )
                    if next_node in not_visited:
                        not_visited.remove(next_node)
                    visited.add( next_node )
        iso_list_list.append( list(visited) )
    return iso_list_list


def graph_create_random(n):
    gadj = [ [0]*n for _ in range(n)]

    for i in  range( n-1 ):
        for j in  range( i+1,n ):
            r = random.randint(0, 10)
            gadj[ i ][ j ] = r
            gadj[ j ][ i ] = r
    return gadj

    for _ in  range( n ):
        from_node = random.randint(0, n-1)
        to_node   = random.randint(0, n-1)
        gadj[from_node][to_node] = random.randint(0, 10)
    graph_make_unidirectional(gadj)

    while True:
        isolated_nodes_list = graph_isolated_nodes(gadj)
        m = len(isolated_nodes_list)
        if m==1:
            break
        for idx in range(m*m):
            i = random.randint(0, m-1)
            j = random.randint(0, m-1)
            szi = len(isolated_nodes_list[i])
            szj = len(isolated_nodes_list[j])
            idxi = random.randint(0, szi-1)
            idxj = random.randint(0, szj-1)
            gadj[ isolated_nodes_list[i][idxi] ][ isolated_nodes_list[j][idxj] ] = random.randint(0,10)

    graph_make_unidirectional(gadj)
    for i in range(len(gadj)):
        gadj[i][i] = 0

    return gadj


if __name__=="__main__":
    n = 2000

    gadj = graph_create_random(n)
#    graph_dump(gadj)
#    print(graph_isolated_nodes(gadj))
    print("start...")
    mx_flow = maxflow_ford_fulkerson(gadj, 0, n-1)
    print("[%d] max flow = %d" % (n, mx_flow) )

    quit()

    gadj = [ [0]*n for _ in range(n)]
    gadj[0][1] = 2
    gadj[0][2] = 1
    gadj[1][2] = 1
    gadj[1][3] = 3
    gadj[2][3] = 2
    graph_make_unidirectional(gadj)
    graph_dump(gadj)
    parent = [-1 for _ in range(n)]
    print( graph_p2p_bfs(gadj, 0, 3, parent) )
    max_flow = maxflow_ford_fulkerson(gadj, 0, 3)
    print(max_flow)

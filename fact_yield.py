def fib_rec(n):
    if n==0:
        return 1
    elif n==1:
        return 1
    else:
        return fib(n-1) + fib(n-2)

def fib_yield(until_n):
    n0 = 0
    n1 = 1
    for i in range(until_n):
        yield n0
        (n1,n0) = (n1+n0, n1)

print(list(fib_yield(10)))

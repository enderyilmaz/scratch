
fin := FileStream open: 'sample.txt' mode: FileStream read.
fin linesDo: [ :line | Transcript nextPutAll: line; nl ].
fin close.

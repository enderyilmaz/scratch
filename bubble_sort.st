bubble_sort := [  :lst | |i j n list|
    list:=lst copy.
    n := list size.
    i:=1.
    [ i<=(n-1) ] whileTrue: [ 
        j:=i+1.
        [ j<=n ] whileTrue: [ 
            ((list at: i)>(list at: j)) ifTrue: [ | temp |
                temp:= list at: i.
                list at: i put: (list at: j).
                list at: j put: temp
                ].
            j:=j+1
            ].
        i:=i+1
        ].
        list do: [:item | Transcript display: item;cr ].
    ].



lst := #(3 2 5 1).


bubble_sort value: lst.
"
Transcript show: lst;cr.
"

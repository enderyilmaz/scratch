#keywords: dynamic-programmin shortest-common-supersequence

def shortest_comm_super_seq_len_rec(txt1, txt2, n1, n2):
    if n1==0 or n2==0:
        return n2+n1
    if txt1[n1-1]==txt2[n2-1]:
        return 1+shortest_comm_super_seq_len_rec(txt1, txt2, n1-1, n2-1)
    else:
        v1 = shortest_comm_super_seq_len_rec(txt1, txt2, n1-1, n2)
        v2 = shortest_comm_super_seq_len_rec(txt1, txt2, n1  , n2-1)
        return min(v1+1,v2+1)
def shortest_comm_super_seq_rec(txt1, txt2, n1, n2):
    if n1==0:
        return txt2[0:n2]
    if n2==0:
        return txt1[0:n1]
    if txt1[n1-1]==txt2[n2-1]:
        return shortest_comm_super_seq_rec(txt1, txt2, n1-1, n2-1) + txt1[n1-1]
    else:
        v1 = shortest_comm_super_seq_rec(txt1, txt2, n1-1, n2)
        v2 = shortest_comm_super_seq_rec(txt1, txt2, n1  , n2-1)
        if len(v1)>len(v2):
            return v2 + txt2[n2-1]
        else:
            return v1 + txt1[n1-1]
def shortest_comm_super_seq_dp(txt1, txt2, n1, n2):
    dp = [ [0]*(n2+1) for _ in range(n1+1)]
    for i in range(n1+1):
        dp[i][0] = i
    for j in range(n2+1):
        dp[0][j] = j
    for i in range(1,n1+1):
        for j in range(1,n2+1):
            if txt1[i-1]==txt2[j-1]:
                dp[i][j] = dp[i-1][j-1]+1
            else:
                v1 = dp[i-1][j] + 1
                v2 = dp[i][j-1] + 1
                dp[i][j] = min(v1,v2)
#    for i in range(n1+1):
#        for j in range(n2+1):
#            print("%3d" % (dp[i][j]), end='')
#        print("")
    return dp[-1][-1]



if __name__=="__main__":
    txt1 = "abc"
    txt2 = "cde"
    n1 = len(txt1)
    n2 = len(txt2)

    print(txt1)
    print(txt2)
    nlcss = shortest_comm_super_seq_rec(txt1, txt2, n1, n2)
    print(nlcss)
    quit()
    nlcss = shortest_comm_super_seq_dp(txt1, txt2, n1, n2)

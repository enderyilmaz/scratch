#keywords longest-increasing-subsequence

import sys

def liss_num_rec(arr, mx, n):
    """ backward O(2^n)
    """
    if n==0:
        return 0
    else:
        v1 = liss_num_rec(arr, mx, n-1)
        v2 = 0
        if arr[n-1]<mx:
            v2 = liss_num_rec(arr, arr[n-1], n-1)  +1
        return max(v1,v2)
def liss_rec(arr, mx, n):
    """ backward O(2^n)

    """
    if n==0:
        return []
    else:
        lst1 = liss_rec(arr, mx, n-1)
        lst2 = []
        if arr[n-1]<mx:
            lst2 = liss_rec(arr, arr[n-1], n-1)
            lst2.extend( [arr[n-1]] )
        if len(lst1)>len(lst2):
            return lst1
        else:
            return lst2
def liss2_num_rec(arr, min_idx, i):
    """ forward O(2^n)
    """
    if i==len(arr):
        return 0
    else:
        min_val = min_idx if min_idx<0 else arr[min_idx]
        v1 = liss2_num_rec(arr, min_idx    , i+1)
        v2 = 0
        if arr[i]>min_val:
            v2 = liss2_num_rec(arr, i, i+1) + 1
        return max(v1,v2)

def liss2_rec(arr, mn, i):
    """ forward O(2^n)
    """
    if i==len(arr):
        return []
    else:
        lst1 = liss2_rec(arr, mn    , i+1)
        lst2 = []
        if arr[i]>mn:
            lst2 = liss2_rec(arr, arr[i], i+1)
            lst2.extend( [ arr[i] ] )
        if len(lst1)>len(lst2):
            return lst1
        else:
            return lst2

def liss_num_On2(a):
    """ dynamic programming. O(n^2)
    """
    n = len(a)
    d = [1 for _ in range(n)]
    for i in range(n):
        for j in range(0,i):
            if (a[j]<a[i]):
                d[i] = max(d[i], d[j]+1)
    mx = d[0]
    for item in d:
        mx = max(mx, item)
    return mx
def liss_On2(a):
    n = len(a)
    d = [1  for _ in range(n)]
    p = [-1 for _ in range(n)]
    for i in range(n):
        for j in range(i):
            if a[j]<a[i] and d[i]<d[j]+1:
                d[i] = d[j] + 1
                p[i] = j
    ans = d[0]
    pos = 0
    for i in range(n):
        if d[i]>ans:
            ans = d[i]
            pos = i

    subseq = []
    while pos != -1:
        subseq.append( a[pos] )
        pos = p[pos]
    subseq.reverse()
    return subseq
def liss_num_Onlogn_(a):
    n = len(a)
    d = [sys.maxsize for _ in range(n+1)]
    di = [-1 for _ in range(n+1)]
    d[0] = -sys.maxsize
    for i in range(n):
        for j in range(1,n):
            if d[j-1]<a[i] and a[i]<=d[j]:
                d[j] = a[i]
                di[j] = i
    for i in range(n+1):
        if d[i]<sys.maxsize:
            ans = i
    return ans

def lis_Onlogn(seq):
    n = len(seq)
    tails = [0]*n
    tails_idx = [-1]*n
    path = [-1]*n
    size = 0
    for idx, x in enumerate(seq):
        i, j = 0, size
        while i!=j:
            m = int( (i+j)/2 )
            if tails[m]<x:
                i = m + 1
            else:
                j = m
        tails[i] = x
        tails_idx[i] = idx
        path[idx] = tails_idx[i-1] if i>0 else -1
        if i==size:
            size = i+1
            listEnd = idx

    lst = []
    while listEnd>=0:
        lst.append(seq[listEnd])
        listEnd = path[listEnd]
    lst.reverse()
    return size, lst
import math
def liss_leet_code_dp(a):
    tails = [0] * len(a)
    size = 0
    for x in a:
        i,j = 0, size
        while i!=j:
            m = int((i+j)/2)
            if tails[m]<x:
                i = m+1
            else:
                j = m
        tails[i] = x
        size = max(i+1, size)
    print(a)
    print(tails)
    return size


if __name__=="__main__":
    arr = [50, 3, 10, 7, 40, 80]
    arr = [3,10,2,1,20]
    print( liss_leet_code_dp(arr) )
    quit()
#    arr = [50, 3, 10]
    n = len(arr)

#    vlis = liss_rec(arr, sys.maxsize, n)
    vlis = liss2_rec(arr, -1, 0)
    nlis = len(vlis)

    nlis = liss_num_Onlogn_(arr)
    vlis = liss_num_Onlogn_(arr)

    print(arr)
    print(nlis)
    print(vlis)


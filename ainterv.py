# Discrete-Event Simulation

from collections import namedtuple

import pytest

"""
In these exercises we'll implement a simple version of a widely-successful paradigm for modeling circuits: the *discrete* event* model.  Specifically we'll supply a model, and you'll write the simulation kernel.

*Discrete-event* is the primary paradigm for the most-popular digital HDLs - Verilog and VHDL.  What does this mean?  We can start by looking at a sample Verilog module:

```
module system_model ;
wire a;
reg state;

initial begin
state = 0;
a = 0;
#21 a = ~a;
end

always @ a begin
state = state + 1;
#11 a = ~a;
end
endmodule
```

This has a few key features:

* *Structural* content
* Here, wires and regs
* (Not captured here) ports, parameters, instances, and the like
* *Procedural* content
* The code in-between the "begin" and "end" clauses

Through this view, the module looks a lot like the classes of our object-oriented software languages.  The structural-components are like class attributes, and the procedural-blocks are like methods.

We can pretty easily imagine a Python class with roughly the same content:
"""

#Signal a
#Signal b
#Signal z
#x1 = Logic_Xor(a,b,z)
class Logic_Xor:
    def __init__(self,sim,a,b,z):
        self.a = a
        self.b = b
        self.z = z
        self.sim = sim
        a.register(self)
        b.register(self)
    def initial(self): pass
    def update(self):
        self.sim.add_event( 
                Event( 
                    time=self.sim.time+1, 
                    task= (lambda : self.z.update( self.a.val ^ self.b.val ) )
                ) 
        )
class Signal: 
    def __init__(self, sid):
        self.load_list = []
        self.val = 0
        self.sid = sid
    def register(self, component):
        self.load_list.append( component )
    def update(self, val):
        print("{}.update({})".format(self.sid, val))
        if self.val != val:
            self.val = val
            for load in self.load_list:
                load.update()

class SystemModel:
    def __init__(self):
        self.sim = Sim()
        e = Event(time=0, task=self.initial)
        self.sim.add_event(e)

    def initial(self):
        self.state = 0
        e = Event(time=21, task=self.update)
        self.sim.add_event(e)

    def update(self):
        self.state += 1
        
        t = self.sim.time  # Note: your sim class should expose this!
        e = Event(time=t + 11, task=self.update)
        self.sim.add_event(e)


"""
Verilog also includes a bunch of syntax for generating delays ("#11") and *when* to run the procedural blocks ("always", "initial"). 
In our exercises we'll think of these as short-hand notations for creating *Events*.  
Next we need to define this "Event" thing. (This is a confusingly over-used term, especially in this context.) 
For our purposes: "Event" will refer to the combination of:

 * A thing to do (i.e. run a procedure), and
 * A (simulated) time to do it 

 More elaborate simulators will have more elaborate categories of events; 
 here we'll stick to the "run this procedure" type. 
 To represent Events we'll use python's `namedtuples`: 
 """

Event = namedtuple('Event', ['time', 'task'])

#
# Example construction (as seen above):
# e = Event(time=0, task=a_function_object)
#
# Example attribute-access:
# e.task()
#


"""
Now all that's missing is your Sim! 
We'll need three methods to make this work:

* Constructor
* No arguments
* Initialize any internal state  
* add_event()
* Adds a (generally future-time) Event
* You'll notice this being called by SystemModel
* Accepts a single argument - an Event
* run(tstop)
* Run the simulation, up to time `tstop`
* Accepts a single argument - a numeric stop-time

Good news: this shouldn't require very much code.
"""


class Sim:
    """ Simulation class
    To be implemented! """

    def __init__(self):
        # Implement me please!
# Hint: Initialize any internal data members
# What internal state would such an object require?
        self.time = 0
        self.schedule = dict()

    def add_event(self, e):
        """ Add a new event
        To be implemented! """
        if not e.time in self.schedule:
            self.schedule[e.time] = []
            self.schedule[e.time].append( e.task )

    def run(self, tstop):
        """ Run the simulation, to simulated time `tstop`
    To be implemented! """
        while self.time < tstop:
            if self.time in self.schedule:
                print("@{}".format( self.time ) )
                for task in self.schedule[self.time]:
                    task()
            self.time += 1


def test_sim_system_model():
    # Instantiate our "DUT"
    model = SystemModel()
# And simulate it!
    model.sim.run(tstop=33)

# Make a few checks
    assert model.state == 2
    assert model.sim.time >= 22
    assert model.sim.time <= 33
    sim = Sim()
    (a,b,z) = (Signal("a"), Signal("b"), Signal("z"))
    xor1 = Logic_Xor(sim=sim,a=a,b=b,z=z)
    sim.add_event(Event(time=1, task=(lambda : a.update(1))))
    sim.run(tstop=5)



"""
When you're done:

Hope that went well.  
There are a few directions to think about extending our work here:

1. What would make this system-model look & feel more like an HDL?  
Probably things like:
* Signals, and sensitivity to them
* Hierarchy; modules which instantiate other modules
* What would it take to add these capabilities?

2. This discrete-event view of our "circuit" is far from unique.
Different simulation-programs will view the circuit very differently. 
It's helpful to have a mental framework for comparing these approaches.
We'll refer to this as the simulation's *model of computation* (MOC). 
Here we explored a digital/ event-driven MOC.  

How can we compare this MOC to others, such as:
* MATLAB-based descriptions of signal-processing components
* Analog circuits and SPICE

We'll talk about the latter in more depth by phone; think about it. 
As a framing mechanism, imagine our circuit no longer has programming-grade behavior, 
but instead looks something like this:


------------/\/\/\-------------
_|_                           _|_
/ ^ \                         / ^ \
\_|_/                         \_|_/
|                             |
-----/\/\/\---------/\/\/\---------/\/\/\-----
_|_            _|_            _|_            _|_
/ ^ \          / ^ \          / ^ \          / ^ \
\_|_/          \_|_/          \_|_/          \_|_/
|              |              |              |
----------------------------------------------
_|_
\_/

(Those are all resistors and current sources.)

How can we programmatically evaluate the node-voltages in this circuit?
For starters - how would you do it with pen & paper? 
Feel free to write up some thoughts.  

"""


if __name__ == '__main__':
    test_sim_system_model()
#    pytest.main([__file__])

